<?php  
defined('BASEPATH') OR exit('No direct script access allowed');

class Laporan_model extends CI_Model {

    public function __construct()
    {
        parent::__construct();
                // Your own constructor code
    }
    public function getTahun()
    {
        $this->db->distinct()
                 ->select("year(_tgl) as tahun")
                 ->from("header_tr a")
                 ->order_by("year(_tgl)","asc");

        return $this->db->get()->result_array();

    }
    public function getLapStok($data)
     {
        $this->db->select("a._kode,b._id_cab,a._nama,a._stok,b._nama_cab,IFnull(c.qtyOut,0) as qtyOut,(_stok-IFnull(c.qtyOut,0)) AS sisa")
                 ->from("produk_ a")
                 ->join("cabang_ b","a._id_cab = b._id_cab","inner")
                 ->join(
                        "(select _id_pro,sum(a._qty) as qtyOut FROM detail_tr a
                            inner join header_tr b on a._id_header=b._id_header
                             where month(b._tgl) = month(now()) group by _id_pro) c",
                        "a._id_pro = c._id_pro",
                        "left"
                    )
                 ->order_by("a._id_cab","asc")
                 ->where("a._jenis",1);
        if (!empty($data)) {
            if (empty($data['order'])) {
                $this->db->like("a._nama",$data['nama']);
                if (!empty($data['cabang'])) {
                    $this->db->where("b._id_cab",$data['cabang']);
                }
            }
            elseif (!empty($data['order'])) {
                $this->db->order_by("b._id_cab","asc");
            }

        }
        return $this->db->get()->result_array();
    }
    public function getLapPenjualanGrid($data){
        $where = "";
         if (!empty($data)) {
            if (!empty($data['nama'])) {
                $this->db->like("a._nama",$data['nama']);
            }
            if (!empty($data['dari'])&&!empty($data['sampai'])){
                $where .= " and b._tgl >= '".$data['dari']."'";
                $where .= " and b._tgl <= '".$data['sampai']."'";
            }else if (empty($data['sampai'])&&!empty($data['dari'])) {
                $where .= " and b._tgl >= '".$data['dari']."'";
            
            }elseif (empty($data['dari'])&&!empty($data['sampai'])) {
                $where .= " and b._tgl <= '".$data['sampai']."'";
            }else{
                $where .= " and b._tgl =  '".date("Y-m-d")."'";
            }
            if (!empty($data['cabang'])) {
                $this->db->where("b._id_cab ",$data['cabang']);
            }
            if (!empty($data['jenis'])) {
                $this->db->where("a._jenis ",$data['jenis']);
            }

        }else{
                $where .= " and b._tgl = '".date("Y-m-d")."'";
            }
        $this->db->select("a._kode,a._jenis,a._harga_jual,b._id_cab,a._nama,a._stok,b._nama_cab,IFnull(c.qtyOut,0) as qtyOut,IFnull(c.totalJual,0) as totalJual,(_stok-IFnull(c.qtyOut,0)) AS sisa")
                 ->from("produk_ a")
                 ->join("cabang_ b","a._id_cab = b._id_cab","inner")
                 ->join(
                        "(select a._id_header,_id_pro,sum(a._total) as totalJual,sum(a._qty) as qtyOut 
                            FROM detail_tr a 
                            inner join header_tr b on a._id_header=b._id_header
                            where 1=1
                            $where
                            group by _id_pro 
                            ) c",
                        "a._id_pro = c._id_pro",
                        "right"
                    )
                 ->join("header_tr d","c._id_header=d._id_header","left")
                 ->order_by("a._id_cab","asc");
       
        return $this->db->get()->result_array();
    }

    public function getLapPenjualan($data,$chart)
    {

        if (empty($chart)) {
            $this->db->select("a._total,b._no_tr,b._tgl,c._nama,c._kode");
        }
        $this->db->from("detail_tr a")
                 ->join("header_tr b","a._id_header=b._id_header")
                 ->join("produk_ c","a._id_pro=c._id_pro")
                 ->join("user_ d","b._id_user=d._id");
        if (!empty($chart)) {
            if (isset($chart['dashboard'])&&$chart['dashboard']) {
                $this->db->select("sum(a._total) as jumlah,MONTH(b._tgl) as bulan")
                     ->group_by("MONTH(b._tgl)")
                     ->order_by("MONTH(b._tgl)","asc");
            }else{
                $this->db->select("sum(a._total) as jumlah,DAY(b._tgl) as bulan")
                     ->group_by("DAY(b._tgl)")
                     ->order_by("DAY(b._tgl)","asc");
                
            }
            if (!empty($chart['jenis'])) {
                $this->db->where("_jenis",$chart['jenis']);
            }
            if (!empty($chart['src'])) {
                
                if(!empty($chart['src']["bulan"])){
                        $this->db->where("month(b._tgl)",$chart['src']["bulan"]);
                }
                if(!empty($chart['src']["tahun"])){
                    $this->db->where("YEAR(b._tgl)",$chart['src']["tahun"]);
                }
            }
        }
        return $this->db->get()->result_array();
    }
    public function getLapPeramalan($data)
    {
        $this->db->select("a.*,b._kode,b._nama,c._nama_cab")
                 ->from("peramalan_ a")
                 ->join("produk_ b","a._id_pro=b._id_pro")

                 ->join("cabang_ c","b._id_cab=c._id_cab")
                 ->where("b._jenis",1)
                 
                 ->order_by("_thn desc,_bln desc");
        if (!empty($data)) {
            $this->db->like("b._nama",$data['nama'])
                     ->like("a._thn",$data['tahun']);
            if(!empty($data['bulan'])){
                $this->db->where("a._bln",$data['bulan']);
            }
            if(!empty($data['cabang'])){
                $this->db->where("b._id_cab",$data['cabang']);
            }
        }
        return $this->db->get()->result_array();


    }
    public function barPenjualan($value='')
    {
        $where = "";
        if (!empty($value['bulan'])) {
            $where .=" and month(b._tgl) =". $value['bulan'];
        }
        if (!empty($value['tahun'])) {
            $where .=" and year(b._tgl) =". $value['tahun'];
        }
        $this->db->select("sum(IFnull(a._total,0)) as total,c._nama_cab")
                 ->from("(select a.* 
                            from detail_tr a
                            inner join header_tr b on a._id_header = b._id_header
                            where 1=1 $where) a")
                 ->join("produk_ b","a._id_pro=b._id_pro")
                 ->join("cabang_ c","b._id_cab=c._id_cab","right")
                 ->group_by("c._id_cab")
                 ->order_by("c._id_cab","asc");
                 // print_r( $this->db->get()->result_array());
                 // die();
        return $this->db->get()->result_array();
    }
     public function barPenjualan2($value='')
    {
        $where = "";
        if (!empty($value['bulan'])) {
            $where .=" and month(b._tgl) =". $value['bulan'];
        }
        if (!empty($value['tahun'])) {
            $where .=" and year(b._tgl) =". $value['tahun'];
        }
        if (!empty($value['jenis'])) {
            $where .=" and c._jenis =". $value['jenis'];
        }
        $this->db->select("sum(IFnull(a._total,0)) as total,c._nama_cab")
                 ->from("(select a.* ,c._id_cab
                             from detail_tr a 
                             inner join header_tr b on a._id_header = b._id_header 
                             inner JOIN `produk_` `c` ON `a`.`_id_pro`=`c`.`_id_pro` 
                             where 1=1  $where) a")
                 ->join("cabang_ c","a._id_cab=c._id_cab","right")
                 ->group_by("c._id_cab")
                 ->order_by("c._id_cab","asc");
                 // print_r( $this->db->get()->result_array());
                 // die();
        
        return $this->db->get()->result_array();
    }
    public function pie_dash($data="",$jenis="")
    {
        $where ="";
        if (!empty($jenis)) {
            $where .= " and c._jenis =".$jenis;
           
        }
        $this->db->select("a._nama,c._nama_cab,a.jumlah")
                 ->join("cabang_ c","a._id_cab=c._id_cab")
                 ->order_by("a.jumlah","desc")
                 ->limit(5,0);
        if (!empty($data)) {
           if(!empty($data["bulan"])){
                $where .= " and month(b._tgl) = ".$data['bulan'];
           }
           if(!empty($data["tahun"])){
                $where .= " and YEAR(b._tgl) = ".$data['tahun'];
           }
           $this->db->from("(select sum(a._qty) as jumlah,c._id_pro,c._nama ,c._id_cab
                                    from detail_tr a 
                                    inner join header_tr b on a._id_header=b._id_header
                                    inner join produk_ c on a._id_pro=c._id_pro

                                    where 1=1
                                    $where
                                    group by c._id_pro) a");
        }else{
                 $this->db->from("(select sum(_qty) as jumlah,_id_pro from detail_tr group by _id_pro) a");
        }
        return $this->db->get()->result_array();

    }
    
    public function card($value='')
    {
        $this->db->select("ifNull(sum(a._qty),0) as produk,count(b._id_header) as transaksi,IFnull(sum(a._total),0) as pendapatan") 
                 ->from("detail_tr a")
                 ->join("header_tr b","a._id_header=b._id_header");
                 if (!empty($value)) {
                    if(!empty($value["bulan"])){
                        $this->db->where("month(b._tgl)",$value["bulan"]);
                    }
                    if(!empty($value["tahun"])){
                        $this->db->where("YEAR(b._tgl)",$value["tahun"]);
                    }
                 }
        return $this->db->get()->result_array();


    }
}	
?>