<?php  
defined('BASEPATH') OR exit('No direct script access allowed');

class User_model extends CI_Model {

    public function __construct()
    {
        parent::__construct();
                // Your own constructor code
    }

    public function getUser()
    {
    	$this->db
    		->select('a.*,b._nama_cab')
    		->from('user_ a')
    		->join('cabang_ b','a._id_cabang=b._id_cab','inner');

    	$query = $this->db->get();
    	return $query->result_array();
    }
    public function edit($id)
    {
    	$query = $this->db->get_where('user_', array('_id' => $id));
    	return $query->row();
    }

    public function update($data)
    {
    	$id = $data['_id'];
    	$this->db->where('_id',$id);
    	$res = $this->db->update('user_',$data);
    	return ($res)?true:false;
    }
    public function insert($data)
    {
    	$res = $this->db->insert('user_', $data);
    	return ($res)?true:false;
    }
    public function delete($id)
    {
    	$res = $this->db->delete('user_', $id);
    	return ($res)?true:false;
    }
    public function loginValidate($data)
    {
        $this->db
            ->select('_id,_group,_nama,_id_cabang') 
            ->from('user_')
            ->where($data);
        $query = $this->db->get();
        return $query->row();
        
    }
}	
?>