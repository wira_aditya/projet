<?php  
defined('BASEPATH') OR exit('No direct script access allowed');

class Peramalan_model extends CI_Model {

    public function __construct()
    {
        parent::__construct();
                // Your own constructor code
    }
   	public function peramalan($thn="",$bln="")
   	{
   		$bulanKemarin1=$bln-1;
		$bulanKemarin2=$bln-2;

		// check bulan
		$this->db->from("header_tr")
				 ->where("MONTH(_tgl)",$bulanKemarin1)
				 ->where("YEAR(_tgl)",$thn);
		$jumlahKemarin = $this->db->count_all_results();


		if($jumlahKemarin > 0){
			$this->db->select("_id_pro")
				 ->from("produk_");
			$item = $this->db->get()->result_array();
			// print_r();
			foreach ($item as $value) {
				$item = $value["_id_pro"];
				//Jual Bulan ini
				$this->db->select_sum("s._qty","total_item")
					     ->from("detail_tr s")
					     ->join("header_tr b","s._id_header=b._id_header","inner")
					     ->where("s._id_pro",$item)
					     ->where("MONTH(b._tgl)",$bln)
					     ->where("YEAR(b._tgl)",$thn);

				$JualIni = $this->db->get()->result()[0]->total_item;
				$JumlahIni=($JualIni > 0) ? $JualIni : 0;
				// jual bulan 1
				$this->db->select_sum("a._qty","total_item")
					     ->from("detail_tr a")
					     ->join("header_tr b","a._id_header=b._id_header","inner")
					     ->where("a._id_pro",$item)
					     ->where("MONTH(b._tgl)",$bulanKemarin1)
					     ->where("YEAR(b._tgl)",$thn);
				$JualKemarin1 = $this->db->get()->result()[0]->total_item;
				$JumlahKemarin1=($JualKemarin1 > 0) ? $JualKemarin1 : 0;


				// jual bulan 2
				$this->db->select_sum("a._qty","total_item")
					     ->from("detail_tr a")
					     ->join("header_tr b","a._id_header=b._id_header","inner")
					     ->where("a._id_pro",$item)
					     ->where("MONTH(b._tgl)",$bulanKemarin2)
					     ->where("YEAR(b._tgl)",$thn);
				$JualKemarin2 = $this->db->get()->result()[0]->total_item;
				$JumlahKemarin2=($JualKemarin2 > 0) ? $JualKemarin2 : 0;


				$F2 = (0.2 * $JumlahKemarin2) + (0.8 * $JumlahKemarin2); //dgn 2 bulan lalu
				$F1 = (0.2*$JumlahKemarin1) + (0.8*$F2); //dgn bulan lalu
				$roundF1 = round($F1, 0, PHP_ROUND_HALF_UP); //bulatkan
				$MAD = $JumlahIni - $roundF1; //bulan ini - peramalan
				$MSE = $MAD*$MAD; // MAD kuadrat
				$MAPE = ($JumlahIni<1) ? 0 : ($MAD/$JumlahIni)*100; // MAD bagi jual bulan inikali 100
				$MAPE = number_format($MAPE, 2, '.', '');
				
				$data = array("_id_pro"=>$item,
								"_bln"=>$bln,
								"_thn"=>$thn,
								"_jual"=>$JumlahIni,
								"_ramalan"=>$roundF1,
								"_mse"=>$MSE,
								"_mad"=>$MAD,
								"_mape"=>$MAPE);
				// check data
				$this->db->select("_id_peramalan")
						 ->from("peramalan_")
						 ->where("_bln",$bln)
						 ->where("_thn",$thn)
						 ->where("_id_pro",$item);
				// $this->db->select("_id_pro")
				//  ->from("pro004005006duk_");
			// $item = $this->db->get()->result()[0]->_id_peramalan;
			// print_r($item);
				$x = $this->db->get()->result();
				// print_r($x);
				if (!empty($x)) {
					$data["_id_peramalan"] = $x[0]->_id_peramalan;
					// print_r($data);
					// die();
					// $res = $this->update($data);
					$res =true;
				}else{
					$res = $this->insert($data);
				}

			}
		}else{
			$res = false;
		}

    	return ($res)?true:false;
			
   	}
   	public function insert($data)
   	{
   		$res = $this->db->insert('peramalan_', $data);
    	return ($res)?true:false;
   	}
   	public function update($data)
   	{
   		$id = $data['_id_peramalan'];
    	$this->db->where('_id_peramalan',$id);
    	$res = $this->db->update('peramalan_',$data);
    	return ($res)?true:false;
   	}
   	public function getPeramalan($thn,$bln)
   	{
   		$this->db->select("a._mse,a._mape,a._mad,c._nama_cab,a._ramalan,b._nama,b._stok")
   				 ->from("peramalan_ a")
   				 ->join("produk_ b","a._id_pro=b._id_pro ")
   				 ->join("cabang_ c","b._id_cab=c._id_cab")
   				 ->where("a._bln",$bln)
   				 ->where("a._thn",$thn)
   				 ->order_by("_thn desc,_bln asc")
				 ->where("b._jenis",1);
   		return $this->db->get()->result_array();
   	}
}	
?>