<?php  
defined('BASEPATH') OR exit('No direct script access allowed');

class Tr_model extends CI_Model {

    public function __construct()
    {
        parent::__construct();
                // Your own constructor code
    }
    public function insertTmp($data)
    {
    	$res = $this->db->insert('temp_tr', $data);
        $success = ($res)?true:false;    
    	$d = $this->getTemp();
    	return array("success"=>$success,"status"=>"insert","data"=>$d);   
    }
    public function updateTmp($data)
    {
    	$id = $data['_id_detail'];
    	$this->db->where('_id_detail',$id);
    	$res = $this->db->update('temp_tr',$data);
    	$success=($res)?true:false;
    	$d = $this->getTemp();
    	return array("success"=>$success,"status"=>"update","data"=>$d);   
    }
    public function getRowTmp($where)
    {
    	$this->db->select("_id_detail,_qty")
				 ->from('temp_tr')
				 ->where($where);
        $query = $this->db->get();
		return $query->row();
    }
    public function getTemp($select="")
    {
        $x = (!empty($select)) ? $select : "a._id_detail,a._qty,a._harga,a._total,a._disk,b._kode,b._nama";
    	$this->db->select($x)
    			 ->from("temp_tr a")
    			 ->join('produk_ b', 'a._id_pro = b._id_pro')
    			 ->where('_user',$this->session->id);
        return (!empty($select)) ? $this->db->get()->result_array() : $this->db->get()->result();
    }
    public function deleteRowTmp($data)
    {
    	$res = $this->db->delete('temp_tr',$data);
        return ($res)?true:false;
    }
    public function deleteTmp()
    {	
    	$this->db->where('_user',$this->session->id);
    	$res = $this->db->delete('temp_tr');
        return ($res)?true:false;
    }
    public function getQty(){
        $this->db
             ->select("sum(_qty) as qty")
             ->from("temp_tr")
             ->where('_user',$this->session->id);
        return $this->db->get()->row();
    }
    public function insert($data)
    {
        $res = $this->db->insert('header_tr', $data);
        $success = ($res)?true:false;
        if ($success) {
            $this->db
                 ->select("_id_header")
                 ->from("header_tr")
                 ->where("_no_tr",$data['_no_tr']);
            $header = $this->db->get()->row()->_id_header;
            $det = $this->getTemp("a._id_header,a._id_pro,a._qty,a._harga,a._disk,a._total");
            foreach ($det as $value) {
                $value['_id_header'] = $header;
                $res = $this->db->insert('detail_tr', $value);
            }
            $this->deleteTmp();

        }    
        return $success;
    }
    public function getData($data)
    {
        // print_r($data);
        $this->db->select("c._nama,b._qty,b._harga,b._total")
                 ->from("header_tr a")
                 ->join("detail_tr b","a._id_header = b._id_header","right")
                 ->join("produk_ c","b._id_pro = c._id_pro")
                 ->where("a._no_tr",$data['notr']);

        return $this->db->get()->result();
        // die();


    }
}   
?>