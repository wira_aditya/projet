<?php  
defined('BASEPATH') OR exit('No direct script access allowed');

class Cabang_model extends CI_Model {

    public function __construct()
    {
        parent::__construct();
                // Your own constructor code
    }
    public function getCabang(){
        $this->db
            ->select('*')
            ->from('cabang_')
            ->order_by("_id_cab","asc")
            ->where("_id_cab !=","0");
        $query = $this->db->get();
        return $query->result_array();
    }
    public function cbCabang()
    {
        $this->db
            ->select('_id_cab,_nama_cab')
            ->from('cabang_')   
            ->order_by('_id_cab',"ASC");
        $query = $this->db->get();
        return $query->result();
    }
    public function edit($id)
    {
        $query = $this->db->get_where('cabang_', array('_id_cab' => $id));
        return $query->row();
    }

    public function update($data)
    {
        $id = $data['_id_cab'];
        $this->db->where('_id_cab',$id);
        $res = $this->db->update('cabang_',$data);
        return ($res)?true:false;
    }
    public function insert($data)
    {
        $res = $this->db->insert('cabang_', $data);
        return ($res)?true:false;
    }
    public function delete($id)
    {
        $res = $this->db->delete('cabang_', $id);
        return ($res)?true:false;
    }
}   
?>