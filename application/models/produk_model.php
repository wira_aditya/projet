<?php  
defined('BASEPATH') OR exit('No direct script access allowed');

class Produk_model extends CI_Model {

    public function __construct()
    {
        parent::__construct();
                // Your own constructor code
    }

    public function getUser($cabang = "",$src)
    {
    	$this->db
    		->select('a.*,b._nama_cab')
    		->from('produk_ a')
    		->join('cabang_ b','a._id_cab=b._id_cab','inner');
        if (!empty($cabang)) {
            $this->db->where("a._id_cab",$cabang);
        }    
        if (!empty($src)) {
            $this->db->like("a._nama",$src['nama'])
                     ->like("a._kode",$src['kode']);
        }
    	$query = $this->db->get();
    	return $query->result_array();
    }
    public function getRowProduct($kode)
    {
        $this->db
            ->select('*')
            ->from('produk_ a');
        if(!empty($kode['id'])){
            $this->db->where("_id_pro",$kode['id']);
        }
        if(!empty($kode['kode'])){
            $this->db->where("_kode",$kode['kode']);
        }

        $query = $this->db->get();
        return $query->row();
    }
    public function edit($id)
    {
    	$query = $this->db->get_where('produk_', array('_id_pro' => $id));
    	return $query->row();
    }

    public function update($data)
    {
    	$id = $data['_id_pro'];
    	$this->db->where('_id_pro',$id);
    	$res = $this->db->update('produk_',$data);
    	return ($res)?true:false;
    }
    public function insert($data)
    {
    	$res = $this->db->insert('produk_', $data);
    	return ($res)?true:false;
    }
    public function delete($id)
    {
    	$res = $this->db->delete('produk_', $id);
    	return ($res)?true:false;
    }
    public function kodeCheck($kode)
    {
        $this->db
            ->select('_id_pro')
            ->from('produk_ a')
            ->where("_kode",$kode);

        $query = $this->db->get()->row();
        return $query->row();
    }
}	
?>