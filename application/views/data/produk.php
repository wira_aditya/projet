   <!-- Content Wrapper. Contains page content -->
  <div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <section class="content-header">
      <h1>Produk</h1>
      <ol class="breadcrumb">
        <li><a><i class="fa fa-cogs"></i>Data Master</a></li>
        <li class="active">Produk</li>
      </ol>
    </section>

    <!-- Main content -->
    <section class="content">
      <div class="row">
        <div class="col-md-12">
          <div class="box  box-success">
            <div class="box-header">
              <h3 class="box-title">Data Produk</h3>
            </div>
            <!-- /.box-header -->
            <div class="box-body">
                <form id="lapStok" role="form" method="get"  class="form-inline" action="<?=base_url()?>produk/">
                  <div class="form-group">
                    <input class="form-control" id="nama" name="nama" value="<?=(!empty($src))?$src['nama']:''?>" type="text" placeholder="Nama Barang">
                  </div>
                  <div class="form-group">
                    <input type="text" id="kode" name="kode" value="<?=(!empty($src))?$src['kode']:''?>" class="form-control" placeholder="Kode Barang">
                  </div>
                  <div class="form-group">
                    <button id="addTrBrg" class="btn btn-success btn-add" type="submit" >
                      <span class="fa fa-search"></span>
                    </button>
                  </div>
                    
                </form>
                <button class="btn btn-primary" data-toggle="modal" id="tambah" data-target="#myModal">Tambah</button>

                <table id="produk-table" class="table table-bordered table-hover" data-id="<?=$this->router->fetch_class()?>">
                  <thead>
                  <tr>
                    <th width="40">No</th>
                    <th>Kode</th>
                    <th>Nama Barang</th>
                    <th>Jenis</th>
                    <th>Stok</th>
                    <th>Harga Pokok</th>
                    <th>Harga jual</th>
                    <th>Cabang</th>
                    <th></th>
                  </tr>
                  </thead>
                  <tbody>
                    <?php $no=1; foreach ($data as $produk) {?>
                      <tr>
                        <td><?=$no?></td>
                        <td><?=$produk['_kode']?></td>
                        <td><?=$produk['_nama']?></td>
                        <td><?=($produk['_jenis'] == 1)?"Barang":"Jasa"?></td>
                        <td><?=$produk['_stok']?></td>
                        <td><?=$produk['_harga_pokok']?></td>
                        <td><?=$produk['_harga_jual']?></td>
                        <td><?=$produk['_nama_cab']?></td>
                        <td>
                          <button id="edit" type="button" class="btn btn-warning btn-edit" data-id="<?=$produk['_id_pro']?>">Edit</button>
                          <button type="button" class="btn btn-danger btn-delete" data-id="<?=$produk['_id_pro']?>">Hapus</button>
                        </td>
                      </tr>
                      <?php $no++;} ?>
                  </tbody>
                </table>
              
            </div>
            <!-- /.box-body -->

          </div> 
        </div>
      </div>
 	  
	  </section>
    <!-- /.content -->
  </div>
  <!-- /.content-wrapper -->
  <div id="myModal" class="modal fade" role="dialog">
  <div class="modal-dialog">

    <!-- Modal content-->
    <form action="<?= base_url()."CProduk/submit"?>" method="post">
      <div class="modal-content">
        <div class="modal-header">
          <button type="button" class="close" data-dismiss="modal">&times;</button>
          <h4 class="modal-title">Produk</h4>
        </div>
        <div class="modal-body">
        <?php 
          if(!empty(validation_errors()) ){
            echo "<div class='error-notif'>
            <span>".validation_errors()."
            </span>
          </div>";
          }
        ?>
        
          <input required="required" type="hidden" name="id_pro">
          <div class="form-group">
            <label>Kode Produk</label>
            <input required="required" value="<?php echo set_value('kode'); ?>" type="text" class="form-control" id="kode" name="kode">
          </div>
          <div class="form-group">
            <label>Nama Produk</label>
            <input required="required" type="text" value="<?php echo set_value('nama'); ?>" class="form-control" id="nama" name="nama">
          </div>
          <div class="form-group">
            <label>Jenis</label>
            <select name="jenis" id="jenis" class="form-control">
              <option <?php echo (set_value('jenis')==1)?"selected":""; ?> value="1">Barang</option>
              <option <?php echo (set_value('jenis')==2)?"selected":""; ?> value="2">Jasa</option>
            </select>
          </div>
          <div class="form-group">
            <label>Stok</label>
            <input value="<?php echo set_value('stok'); ?>" type="text" class="form-control" id="stok" name="stok">
          </div>
          <div class="form-group">
            <label>Harga Pokok</label>
            <input value="<?php echo set_value('harga_pokok'); ?>" required="required" type="number" class="form-control" id="harga_pokok" name="harga_pokok">
          </div>
          <div class="form-group">
            <label>Harga Jual</label>
            <input value="<?php echo set_value('harga_jual'); ?>" required="required" type="number" class="form-control" id="harga_jual" name="harga_jual">
          </div>
          <div class="form-group">
            <label>Cabang</label>
            <select name="id_cab" id="cabang" class="form-control">
              <?php foreach ($cbCabang as $value) { ?>
                <option <?php echo (set_value('jenis')==$value->_id_cab)?"selected":""; ?> value="<?=$value->_id_cab?>"><?=$value->_nama_cab?></option>
              <?php } ?>
            </select>
          </div>
        </div>
        <div class="modal-footer">
          <button type="submit" class="btn btn-primary">Simpan</button>
          <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
        </div>
      </div>
    </form>

  </div>
</div>
  <script>
    $(function () {
      $('#produk-table').DataTable({
        "paging": true,
        "lengthChange": false,
        "searching": false,
        "ordering": true,
        "info": true,
        "autoWidth": false
      });
    });
  </script>
  <script>
    $("#tambah").click(function() {
      $(".error-notif").remove();
      $(".form-group>#kode").removeAttr("readonly");
    })
    $('.btn-edit').click(function() {
      console.log("asdasdasdsadas2");
      $(".form-group>#kode").attr("readonly",true);
      $(".error-notif").remove();
    })
  </script>
  <?php  if (!empty(validation_errors()) ) :?>
  <script>
  $("#myModal").modal("show");
  </script>
<?php endif;?>