  <ul class="sidebar-menu">
        <li class="header"><b>List Menu</b></li>
        <!-- Optionally, you can add icons to the links -->
        <?php  
        if($this->session->group == 1){ ?>
            <li <?php echo ($menu =="dashboard") ? "class='active'":""?>><a href="<?=base_url()?>"><i class="fa fa-tachometer"></i> <span>Dashboard</span></a></li>
            <li class="treeview <?php echo ($menu =="lpPenjualan"||$menu == "lpStok"||$menu == "lpPeramalan") ? "active":""?>">
              <a href="#"><i class="fa fa-print"></i> <span>Laporan</span>
                <span class="pull-right-container">
                  <i class="fa fa-angle-left pull-right"></i>
                </span>
              </a>
              <ul class="treeview-menu">
                <li <?php echo ($menu =="lpStok") ? "class='active'":""?>><a href="<?=base_url()?>laporan-stok/"><i class="fa fa-file-text"></i>Stok</a></li>
                <li <?php echo ($menu =="lpPenjualan") ? "class='active'":""?>><a href="<?=base_url()?>laporan-penjualan/"><i class="fa fa-file-text"></i>Penjualan</a></li>
                <li <?php echo ($menu =="lpPeramalan") ? "class='active'":""?>><a href="<?=base_url()?>laporan-peramalan/"><i class="fa fa-file-text"></i>Peramalan</a></li>
              </ul>
            </li>
        <?php  }elseif ($this->session->group == 2) { ?>
         
            <li <?php echo ($menu =="dashboard") ? "class='active'":""?>><a href="<?=base_url()?>"><i class="fa fa-tachometer"></i> <span>Dashboard</span></a></li>
            <li class="treeview <?php echo ($menu =="user"||$menu == "produk"||$menu == "cabang") ? "active":""?>" >
              <a href="#"><i class="fa fa-cogs"></i> <span>Data Master</span>
                <span class="pull-right-container">
                  <i class="fa fa-angle-left pull-right"></i>
                </span>
              </a>
              <ul class="treeview-menu">
                <li <?php echo ($menu =="user") ? "class='active'":""?>><a href="<?=base_url()?>user/"><i class="fa fa-user"></i>User</a></li>
                <li <?php echo ($menu =="produk") ? "class='active'":""?>><a href="<?=base_url()?>produk/"><i class="fa fa-shopping-bag"></i>Produk</a></li>
                <li <?php echo ($menu =="cabang") ? "class='active'":""?>><a href="<?=base_url()?>cabang/"><i class="fa fa-users "></i>Cabang</a></li>
              </ul>
            </li>
            
            <li <?php echo ($menu =="peramalan") ? "class='active'":""?>><a href="<?=base_url()?>peramalan/"><i class="fa fa-calendar "></i> <span>Peramalan</span></a></li>
        <?php }elseif ($this->session->group == 3) {?>
           <li <?php echo ($menu =="transaksi") ? "class='active'":""?>><a href="<?=base_url()?>transaksi/"><i class="fa fa-tag"></i> <span>POS(transaksi)</span></a></li>
       <?php } ?>
</ul>
        
       