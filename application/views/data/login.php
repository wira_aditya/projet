<?php  
	if ($this->session->has_userdata('id')) {
			redirect(base_url(),'refresh');
	}
?>
<!DOCTYPE html>
<html lang="en">
<head>
	<meta charset="UTF-8">
	<title>Login</title>
	<meta content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no" name="viewport">
	<link rel="stylesheet" href="<?=base_url()?>assets/css/bootstrap.min.css">
	<link rel="stylesheet" href="<?=base_url()?>assets/css/style.css">
	<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/font-awesome/4.7.0/css/font-awesome.min.css">
  <link rel="stylesheet" href="<?=base_url()?>assets/css/skin-red.min.css">
  <script src="https://cdnjs.cloudflare.com/ajax/libs/jquery/2.2.3/jquery.min.js"></script>
</head>
<body class="body-log">
<div class="container">
	<div class="col-sm-4 col-xs-10 card-login">
		<div class='user-box'>
		    <div class='user-content'></div>
		</div> 
		<?php 
		if(count($this->session->flashdata('error')) ){
			echo "<div class='error-notif'>
			<span>".$this->session->flashdata('error')."
			</span>
		</div>";
		}
		?>
		
		<form action="<?= base_url()."CLogin/loginCheck"?>" method="post">
			<div class="form-group">
				<div class="input-group">
					<span class="input-group-addon" id="basic-addon1">
						<i class="fa fa-user" aria-hidden="true"></i>
					</span>
						<input type="text" required="required" required="" name="un" class="form-control padding-input" placeholder="Username">
				</div>
			</div>
			<div class="form-group">
					<div class="input-group">
						<span class="input-group-addon" id="basic-addon1">
							<i class="fa fa-lock" aria-hidden="true"></i>
						</span>
						<input type="password" required="required" name="pw" required="" class="form-control padding-input" placeholder="Password">
					</div>
			</div>
			<div class="form-group">
				<button  class="btn btn-danger floatRight btn-login" type="submit">Log In <i class="fa fa-caret-square-o-right" aria-hidden="true"></i></button>
			</div>
		</form>
	</div>
</div>
	
</body>
</html>