<style>
	table{border: 1px solid #000;border-collapse: collapse;}
	tr>th{padding:10px 50px}
	tr>td{padding: 10px}
  th{background-color: #eaeaea}
	h3{text-align: center;padding-top: 50px}
  .img {position: absolute;
        width: 100px;
        height: 100px;
        background-image: url("../assets/img/logo.png");
        background-size:     cover;                      /* <------ */
        background-repeat:   no-repeat;}
</style>
<div class="img"></div>
	<h3>LAPORAN STOK <?=strtoupper($cabang)?></h3>
<table id="peramalan-table" border="1">
  <thead>
  <tr>
    <th width="20">No</th>
    <th>Kode</th>
    <th>Nama Barang</th>
    <th>Cabang</th>
    <!-- <th>Stok Input</th>
    <th>Stok Out</th> -->
    <th>Stok</th>
  </tr>
  </thead>
  <tbody>
    <?php  
      $no =0;
      if (!empty($data)):
      foreach ($data as $value) :
      
    ?>
      <tr>
        <td><?=++$no?></td>
        <td><?=$value['_kode']?></td>
        <td><?=$value['_nama']?></td>
        <td><?=$value['_nama_cab']?></td>
        <td><?=$value['sisa']?></td>
      </tr>
    <?php endforeach;endif; ?>
  </tbody>
</table>
<script>
	window.print();
	setTimeout(window.close, 200);
</script>