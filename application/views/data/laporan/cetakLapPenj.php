<style>
	table{border: 1px solid #000;border-collapse: collapse;}
	tr>th{padding:10px 50px;background-color: #eaeaea;}
	tr>td{padding: 10px}
	h3{text-align: center;padding-top: 50px}
  .img {position: absolute;
        width: 100px;
        height: 100px;
        background-image: url("../assets/img/logo.png");
        background-size:     cover;                      /* <------ */
        background-repeat:   no-repeat;}
</style>
<?php function changeFormatDate($value)
{
return date("d-m-Y", strtotime($value));
} ?>
<div class="img"></div>

	<h3>LAPORAN PENJUALAN <?=strtoupper($cabang)?></h3>
   <span>Dari Tanggal : <?=(!empty($this->input->get()['dari']))? changeFormatDate($this->input->get()['dari']) : changeFormatDate(date("Y-m-d")) ?></span> 
   <br><span>Sampai Tanggal : <?=(!empty($this->input->get()['sampai']))? changeFormatDate($this->input->get()['sampai']) : changeFormatDate(date("Y-m-d")) ?></span> 
<table id="peramalan-table" border="1">
  <thead>
  <tr>
    <th width="100">No</th>
      <th>Kode Produk</th>
      <th>Nama Produk</th>
      <th>Jenis</th>
      <th>Cabang</th>
      <th>Stok In</th>
      <th>Stok Out</th>
      <th>Sisa</th>
      <th>Harga Jual</th>
      <th>Penjualan</th>
  </tr>
  </thead>
  <tbody>
    <?php  
      $no =0;
	  $ttl =0;
      if (!empty($data)):
      foreach ($data as $value) :
      $ttl+=$value['totalJual'];
    ?>
      <tr>
        <td><?=++$no?></td>
        <td><?=$value['_kode']?></td>
        <td><?=$value['_nama']?></td>
        <td><?=($value['_jenis']==1)?"Barang":"Jasa"?></td>
        <td><?=$value['_nama_cab']?></td>
        <td><?=$value['_stok']?></td>
        <td><?=$value['qtyOut']?></td>
        <td><?=$value['sisa']?></td>
        <td><?=$value['_harga_jual']?></td>
        <td><?=$value['totalJual']?></td>
      </tr>
    <?php endforeach;endif; ?>
	<tr>
		<td colspan="7">Total Penjualan</td>
		<td><?=$ttl?></td>
	</tr>
  </tbody>
</table>
<script>
	window.print();
	setTimeout(window.close, 200);
</script>