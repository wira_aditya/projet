<style>
	table{border: 1px solid #000;border-collapse: collapse;}
  tr>th{padding:10px 50px;background-color: #eaeaea;}
  tr>td{padding: 10px}
  h3{text-align: center;padding-top: 50px}
  .img {position: absolute;
        width: 100px;
        height: 100px;
        background-image: url("../assets/img/logo.png");
        background-size:     cover;                      /* <------ */
        background-repeat:   no-repeat;}
        @page { size: landscape; }
</style>
<div class="img"></div>

	<h3>LAPORAN PERAMALAN <?=strtoupper($cabang)?></h3>
<table id="peramalan-table" border="1">
  <thead>
  <tr>
    <th width="100">No</th>
    <th>Kode Barang</th>
    <th>Nama Barang</th>
    <th>Cabang</th>
    <th>Tahun</th>
    <th>Bulan</th>
    <th>Stok Yang Disediakan</th>
    <th>Mean Squared Error</th>
    <th>Mean Absolute Percentage Error</th>
    <th>Mean Absolute Deviation</th>
  </tr>
  </thead>
  <tbody>

    <?php  
      $no =0;
      if (!empty($data)):
      foreach ($data as $value) :
      
    ?>
      <tr>
        <td><?=++$no?></td>
        <td><?=$value['_kode']?></td>
        <td><?=$value['_nama']?></td>
        <td><?=$value['_nama_cab']?></td>
        <td><?=$value['_thn']?></td>
        <td><?=$bln[$value['_bln']-1]?></td>
        <td><?=$value['_ramalan']?></td>
        <td><?=($value['_mse']<0)?str_replace("-", "(", $value['_mse']).")":$value['_mse']?></td>
        <td><?=($value['_mape']<0)?str_replace("-", "(", $value['_mape']).")":$value['_mape']?>%</td>
        <td><?=$value['_mad']?></td>
      </tr>
    <?php endforeach;endif; ?>
  </tbody>
</table>
<script>
	window.print();
	setTimeout(window.close, 200);
</script>