<!DOCTYPE html>
<html>
<head>
	<title> </title>
	<style>
		@font-face {
		    font-family: dot;
		    src: url(fakereceipt.ttf);
		}
		*{font-family: 'dot';font-size: 7px;padding: 0;margin: 0}
		.title{font-size: 10px}
		.subtitle{font-size: 7px;margin-bottom: 20px}
		table{
			border-top: 1px dashed #000;
			width: 100%;
			margin-top: 5px;
		}
		tr>td{text-align: right}
		tr>td:first-child{text-align: left;}
		.total-border{border-top: 1px dashed #000}
		.text-left{text-align: left;}
	</style>
</head>
<body>
	<center>
		<p class="title">VEGAS BARBERSHOP</p>
		<!-- <p class="subtitle">Jalan Pantai kuta no 12</p> -->
	</center>
	<?php  
	// print_r($this->session->all_userdata());
	// die();
	?>
	<p>Nomor : <?=$header['notr']?></p>
	<p>Tanggal : <?=date("d-m-Y")?></p>
	<p>kasir : <?=$this->session->userdata['nama']?></p>
	<table>
		<?php foreach ($det as $value) :?>
			<tr>
				<td><?=$value->_nama?></td>
				<td><?=$value->_qty?></td>
				<td><?=number_format($value->_harga,0,",",".")?></td>
				<td><?=number_format($value->_total,0,",",".")?></td>
			</tr>
			
		<?php endforeach; ?>
		<tr>
				<td></td>
				<td></td>
				<td class="total-border text-left">Total</td>
				<td class="total-border"><?=number_format($header['grandTotal'],0,",",".")?></td>
			</tr>
			<tr>
				<td></td>
				<td></td>
				<td class="text-left">Tunai</td>
				<td><?=number_format($header['bayar'],0,",",".")?></td>
			</tr>
			<tr>
				<td></td>
				<td></td>
				<td class="text-left">kembali</td>
				<td><?=number_format($header['kembali'],0,",",".")?></td>
			</tr>
	</table>

</body>
</html>

<script>
	window.print();
	setTimeout(window.close, 200);
</script>