<?php 
		$title = array(
		    'font' => array(
		        'bold'  => true,
		        'size'  => 20,
		        'name'  => 'Verdana'
		    ));
		$th = array(
		    'font' => array(
		        'bold'  => true,
		        'name'  => 'Verdana'
		    ));
		$objPHPExcel = new PHPExcel();
		$objPHPExcel->getActiveSheet()->getStyle('A1')->applyFromArray($title);
		$objPHPExcel->getActiveSheet()->getStyle('A2:J2')->applyFromArray($th);
		$objPHPExcel->setActiveSheetIndex(0);
		$objPHPExcel->getActiveSheet()
					->mergeCells('A1:J1')
					->setCellValue('A1','LAPORAN PERAMALAN '.$cabang)
					->setCellValue('A2','No')
					->setCellValue('B2','Kode Barang')
					->setCellValue('C2','Nama Barang')
					->setCellValue('D2','Cabang')
					->setCellValue('E2','Tahun')
					->setCellValue('F2','Bulan')
					->setCellValue('G2','Stok Yang Disediakan')
					->setCellValue('H2','Mean Squared Error')
					->setCellValue('I2','Mean Absolute Percentage Error')
					->setCellValue('J2','Mean Absolute Deviation');
		$objPHPExcel->getActiveSheet()->getRowDimension('1')->setRowHeight(40);
		$i=0;
		// print_r($data);
		// die();
		foreach ($data as $value) {
			++$i;
			$mse = ($value['_mse']<0)?str_replace("-", "(", $value['_mse']).")":$value['_mse'];
			$mape = ($value['_mape']<0)?str_replace("-", "(", $value['_mape'])."%)":$value['_mape'];
			$objPHPExcel->getActiveSheet()
					->setCellValue('A'.(2+$i),$i)
					->setCellValue('B'.(2+$i),$value['_kode'])
					->setCellValue('C'.(2+$i),$value['_nama'])
					->setCellValue('D'.(2+$i),$value['_nama_cab'])
					->setCellValue('E'.(2+$i),$value['_thn'])
					->setCellValue('F'.(2+$i),$bln[$value['_bln']-1])
					->setCellValue('G'.(2+$i),$value['_ramalan'])
					->setCellValue('H'.(2+$i),$mse)
					->setCellValue('I'.(2+$i),$mape."%")
					->setCellValue('J'.(2+$i),$value['_mad'])
					->getStyle('A2:J2')->applyFromArray(
					    array(
					        'fill' => array(
					            'type' => PHPExcel_Style_Fill::FILL_SOLID,
					            'color' => array('rgb' => 'EAEAEA')
					        )
					    )
					);
		}

		$objPHPExcel->getActiveSheet()->setTitle('Laporan Peramalan');
		$objPHPExcel->getActiveSheet()->getStyle('A1')->getAlignment()->applyFromArray(
		    array('horizontal' => PHPExcel_Style_Alignment::HORIZONTAL_CENTER,)
		);
		$styleArray = array(
		  'borders' => array(
		    'allborders' => array(
		      'style' => PHPExcel_Style_Border::BORDER_THIN
		    )
		  )
		);

		$objPHPExcel->getActiveSheet()->getStyle('A2:J'.(2+$i))->applyFromArray($styleArray);
		unset($styleArray);
	    
		$objPHPExcel->getActiveSheet()->getRowDimension('2')->setRowHeight(40);
		$objPHPExcel->getActiveSheet()->getDefaultColumnDimension()->setWidth(20);
		$objPHPExcel->getActiveSheet()->getColumnDimension('A')->setWidth(5);
		$objPHPExcel->getActiveSheet()->getColumnDimension('I')->setWidth(25);
		$objPHPExcel->getActiveSheet()->getColumnDimension('I')->setWidth(40);
		$objPHPExcel->getActiveSheet()->getColumnDimension('J')->setWidth(40);
		$objPHPExcel->getActiveSheet()->getColumnDimension('E')->setWidth(25);
		


		$objDrawing = new PHPExcel_Worksheet_Drawing();
		$objDrawing->setName('test_img');
		$objDrawing->setDescription('test_img');
		$objDrawing->setPath('assets/img/logo.png');
		$objDrawing->setCoordinates('A1');                      
		//setOffsetX works properly
		$objDrawing->setOffsetX(5); 
		$objDrawing->setOffsetY(5);                
		//set width, height
		$objDrawing->setWidth(300); 
		$objDrawing->setHeight(60); 
		$objDrawing->setWorksheet($objPHPExcel->getActiveSheet());
	    ob_end_clean();
		// Redirect output to a client’s web browser (Excel2007)
		header('Content-Type: application/vnd.openxmlformats-officedocument.spreadsheetml.sheet');
		header('Content-Disposition: attachment;filename="laporanPeramalan.xlsx"');
		header('Cache-Control: max-age=0');
		// If you're serving to IE 9, then the following may be needed
		header('Cache-Control: max-age=1');

		// If you're serving to IE over SSL, then the following may be needed
		header ('Expires: Mon, 26 Jul 1997 05:00:00 GMT'); // Date in the past
		header ('Last-Modified: '.gmdate('D, d M Y H:i:s').' GMT'); // always modified
		header ('Cache-Control: cache, must-revalidate'); // HTTP/1.1
		header ('Pragma: public'); // HTTP/1.0

		$objWriter = PHPExcel_IOFactory::createWriter($objPHPExcel, 'Excel2007');
		$objWriter->save('php://output');
		exit;
?>