<?php 
		$title = array(
		    'font'  => array(
		        'bold'  => true,
		        'size'  => 20,
		        'name'  => 'Verdana'
		    ));
		$th = array(
		    'font'  => array(
		        'bold'  => true,
		        'name'  => 'Verdana'
		    ));


		$objPHPExcel = new PHPExcel();
		
		$objDrawing = new PHPExcel_Worksheet_Drawing();
		$objDrawing->setName('test_img');
		$objDrawing->setDescription('test_img');
		$objDrawing->setPath('assets/img/logo.png');
		$objDrawing->setCoordinates('A1');                      
		//setOffsetX works properly
		$objDrawing->setOffsetX(5); 
		$objDrawing->setOffsetY(5);                
		//set width, height
		$objDrawing->setWidth(300); 
		$objDrawing->setHeight(60); 
		$objDrawing->setWorksheet($objPHPExcel->getActiveSheet());
		
		$objPHPExcel->getActiveSheet()->getStyle('A1')->applyFromArray($title);
		$objPHPExcel->getActiveSheet()->getStyle('A2:E2')->applyFromArray($th);
		$objPHPExcel->setActiveSheetIndex(0);
		$objPHPExcel->getActiveSheet()
					->mergeCells('A1:E1')
					->setCellValue('A1','LAPORAN STOK '.strtoupper($cabang))
					->setCellValue('A2','No')
					->setCellValue('B2','Kode')
					->setCellValue('C2','Nama Barang')
					->setCellValue('D2','Cabang')
					->setCellValue('E2','Stok')
					->getStyle('A2:E2')->applyFromArray(
					    array(
					        'fill' => array(
					            'type' => PHPExcel_Style_Fill::FILL_SOLID,
					            'color' => array('rgb' => 'EAEAEA')
					        )
					    )
					);
		$objPHPExcel->getActiveSheet()->getRowDimension('1')->setRowHeight(50);
		$objPHPExcel->getActiveSheet()->getRowDimension('2')->setRowHeight(40);
		$objPHPExcel->getActiveSheet()->getDefaultColumnDimension()->setWidth(20);
		$objPHPExcel->getActiveSheet()->getColumnDimension('A')->setWidth(10);
		$i=0;
		foreach ($data as $value) {
			++$i;
			$objPHPExcel->getActiveSheet()
					->setCellValue('A'.(2+$i),$i)
					->setCellValue('B'.(2+$i),$value['_kode'])
					->setCellValue('C'.(2+$i),$value['_nama'])
					->setCellValue('D'.(2+$i),$value['_nama_cab'])
					->setCellValue('E'.(2+$i),$value['sisa']);
		}

		$objPHPExcel->getActiveSheet()->setTitle('Laporan Stok');
		$objPHPExcel->getActiveSheet()->getStyle('A1')->getAlignment()->applyFromArray(
		    array('horizontal' => PHPExcel_Style_Alignment::HORIZONTAL_CENTER,)
		);
		$styleArray = array(
		  'borders' => array(
		    'allborders' => array(
		      'style' => PHPExcel_Style_Border::BORDER_THIN
		    )
		  )
		);

		$objPHPExcel->getActiveSheet()->getStyle('A2:E'.(2+$i))->applyFromArray($styleArray);
		unset($styleArray);
		// Redirect output to a client’s web browser (Excel2007)
		header('Content-Type: application/vnd.openxmlformats-officedocument.spreadsheetml.sheet');
		header('Content-Disposition: attachment;filename="laporanStok.xlsx"');
		header('Cache-Control: max-age=0');
		// If you're serving to IE 9, then the following may be needed
		header('Cache-Control: max-age=1');

		// If you're serving to IE over SSL, then the following may be needed
		header ('Expires: Mon, 26 Jul 1997 05:00:00 GMT'); // Date in the past
		header ('Last-Modified: '.gmdate('D, d M Y H:i:s').' GMT'); // always modified
		header ('Cache-Control: cache, must-revalidate'); // HTTP/1.1
		header ('Pragma: public'); // HTTP/1.0

		$objWriter = PHPExcel_IOFactory::createWriter($objPHPExcel, 'Excel2007');
		$objWriter->save('php://output');
		exit;
?>