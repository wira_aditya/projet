<?php  
function changeFormatDate($value)
{
return date("d-m-Y", strtotime($value));
}
		$title = array(
		    'font'  => array(
		        'bold'  => true,
		        'size'  => 20,
		        'name'  => 'Verdana'
		    ));
		$th = array(
		    'font'  => array(
		        'bold'  => true,
		        'name'  => 'Verdana'
		    ));
		// $data =  $this->input->get();
		// $data = $this->Laporan_model->getLapPenjualan($data);
		$objPHPExcel = new PHPExcel();

		$objDrawing = new PHPExcel_Worksheet_Drawing();
		$objDrawing->setName('test_img');
		$objDrawing->setDescription('test_img');
		$objDrawing->setPath('assets/img/logo.png');
		$objDrawing->setCoordinates('A1');                      
		//setOffsetX works properly
		$objDrawing->setOffsetX(5); 
		$objDrawing->setOffsetY(5);                
		//set width, height
		$objDrawing->setWidth(300); 
		$objDrawing->setHeight(60); 
		$objDrawing->setWorksheet($objPHPExcel->getActiveSheet());
		
		$objPHPExcel->getActiveSheet()->getDefaultColumnDimension()->setWidth(20);
		$objPHPExcel->getActiveSheet()->getStyle('A1')->applyFromArray($title);
		$objPHPExcel->getActiveSheet()->getStyle('A4:J4')->applyFromArray($th);
		$objPHPExcel->setActiveSheetIndex(0);
		$objPHPExcel->getActiveSheet()
					->mergeCells('A1:G1')
					->setCellValue('A1','LAPORAN PENJUALAN '.strtoupper($cabang))
					
					->setCellValue('A2','Dari Tanggal')
					->setCellValue('A3','Sampai Tanggal')
					->setCellValue('B2',(!empty($this->input->get()['dari']))? changeFormatDate($this->input->get()['dari']) : changeFormatDate(date("Y-m-d")))
					->setCellValue('B3',(!empty($this->input->get()['sampai']))? changeFormatDate($this->input->get()['sampai']) : changeFormatDate(date("Y-m-d")))

					->setCellValue('A4','No')
					->setCellValue('B4','Kode Produk')
					->setCellValue('C4','Nama Produk')
					->setCellValue('D4','Jenis')
					->setCellValue('E4','Cabang')
					->setCellValue('F4','Stok In')
					->setCellValue('G4','Stok Out')
					->setCellValue('H4','Sisa')
					->setCellValue('I4','Harga Jual')
					->setCellValue('J4','Penjualan')
					->getStyle('A4:J4')->applyFromArray(
					    array(
					        'fill' => array(
					            'type' => PHPExcel_Style_Fill::FILL_SOLID,
					            'color' => array('rgb' => 'EAEAEA')
					        )
					    )
					);
		$objPHPExcel->getActiveSheet()->getRowDimension('1')->setRowHeight(40);
		$objPHPExcel->getActiveSheet()->getRowDimension('4')->setRowHeight(40);

		$i=0;
		$ttl = 0;
		foreach ($data as $value) {
			++$i;
			$objPHPExcel->getActiveSheet()
					->setCellValue('A'.(4+$i),$i)
					->setCellValue('B'.(4+$i),$value['_kode'])
					->setCellValue('C'.(4+$i),$value['_nama'])
					->setCellValue('D'.(4+$i),($value['_jenis']==1)?"Barang":"Jasa")
					->setCellValue('E'.(4+$i),$value['_nama_cab'])
					->setCellValue('F'.(4+$i),$value['_stok'])
					->setCellValue('G'.(4+$i),$value['qtyOut'])
					->setCellValue('H'.(4+$i),$value['sisa'])
					->setCellValue('I'.(4+$i),$value['_harga_jual'])
					->setCellValue('J'.(4+$i),$value['totalJual']);
			$ttl = $value['totalJual'];
		}
		$objPHPExcel->getActiveSheet()
			->mergeCells('A'.(5+$i).':G'.(5+$i))
			->setCellValue('A'.(5+$i),"Total Penjualan")
			->setCellValue('J'.(5+$i),$ttl);

		$objPHPExcel->getActiveSheet()->setTitle('Laporan Penjualan');
		$objPHPExcel->getActiveSheet()->getStyle('A1')->getAlignment()->applyFromArray(
		    array('horizontal' => PHPExcel_Style_Alignment::HORIZONTAL_CENTER,)
		);
		$styleArray = array(
		  'borders' => array(
		    'allborders' => array(
		      'style' => PHPExcel_Style_Border::BORDER_THIN
		    )
		  )
		);

		$objPHPExcel->getActiveSheet()->getStyle('A4:J'.(5+$i))->applyFromArray($styleArray);
		unset($styleArray);
		// Redirect output to a client’s web browser (Excel2007)
		header('Content-Type: application/vnd.openxmlformats-officedocument.spreadsheetml.sheet');
		header('Content-Disposition: attachment;filename="laporanPenjualan.xlsx"');
		header('Cache-Control: max-age=0');
		// If you're serving to IE 9, then the following may be needed
		header('Cache-Control: max-age=1');

		// If you're serving to IE over SSL, then the following may be needed
		header ('Expires: Mon, 26 Jul 1997 05:00:00 GMT'); // Date in the past
		header ('Last-Modified: '.gmdate('D, d M Y H:i:s').' GMT'); // always modified
		header ('Cache-Control: cache, must-revalidate'); // HTTP/1.1
		header ('Pragma: public'); // HTTP/1.0

		$objWriter = PHPExcel_IOFactory::createWriter($objPHPExcel, 'Excel2007');
		$objWriter->save('php://output');
		exit;
?>