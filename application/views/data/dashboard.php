   <!-- Content Wrapper. Contains page content -->
  <div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <section class="content-header">
      <h1>Home</h1>
      <ol class="breadcrumb">
        <li><a href="#"><i class="fa fa-dashboard"></i>Home</a></li>
      </ol>
    </section>

    <!-- Main content -->

      <?php if($this->session->group ==2): ?>
    <section class="content">
      <div class="row"> <!-- box kosong --> 
        <div class="col-md-12">
          <div class="box box-primary">
            <div class="box-header with-border">
              <h3 class="box-title"></h3>
            </div><!-- /.box-header -->
            <div class="box-body">
      				<div style="width:500px;margin:0 auto;height:500px">
      				  <img src="<?=base_url()?>/assets/img/logo.png" style="margin:0 auto" alt="User Image">
      				</div>
      			</div><!-- /.box-body -->
          </div><!-- /.box -->
        </div>
      </div>
    </section>
      <?php  else:?>
    <section class="content">
 	    <div class="row"> <!-- box kosong --> 
        <div class="col-md-12">
          <div class="box box-primary">
            <div class="box-header with-border">
              <h3 class="box-title"></h3>
            </div><!-- /.box-header -->
            <div class="box-body">
              <form id="dashboardSrc" style="width: 350px; margin: auto;" role="form" method="get"  class="form-inline" action="<?=base_url()?>dashboard/">
                <div class="form-group">
                  <!-- <input class="form-control" id="tahun" name="tahun" value="<?=$src['tahun']?>" type="number" placeholder="Tahun"> -->
                  
                  <select class="form-control" name="tahun" id="tahun">
                    <?php foreach ($tahun as $key => $value): ?>
                      <option value="<?=$value['tahun']?>" <?=($src['tahun'] == $value['tahun'])?"selected":""  ?>><?=$value['tahun']?></option>
                    <?php endforeach ?>
                  </select>
                </div> 
                <div class="form-group">
                  <select class="form-control" name="bulan" id="bulan">
                    <option value="">Bulan</option>
                    <?php foreach ($bln as $key => $value): ?>
                      <option value="<?=$key+1?>" <?=($src['bulan'] == $key+1)?"selected":""  ?>><?=$value?></option>
                    <?php endforeach ?>
                  </select>
                </div>
                 
                <div class="form-group">
                  <button id="cari" class="btn btn-success btn-add" type="submit" >
                    <span class="fa fa-search"></span>
                  </button>
                </div>
              </form>
            </div><!-- /.box-body -->
          </div><!-- /.box -->
        </div>
      </div>
      <div class="row">
        
        <div class="col-sm-4" >
          <div class="info-box">
            <!-- Apply any bg-* class to to the icon to color it -->
            <span class="info-box-icon bg-aqua"><i class="fa fa-shopping-bag"></i></span>
            <div class="info-box-content">
              <span class="info-box-text">Produk terjual</span>
              <span class="info-box-number" id="produk-card">0</span>
            </div><!-- /.info-box-content -->
          </div><!-- /.info-box -->
        </div>
        <div class="col-sm-4" >
          <div class="info-box">
            <!-- Apply any bg-* class to to the icon to color it -->
            <span class="info-box-icon bg-yellow"><i class="fa fa-tag"></i></span>
            <div class="info-box-content">
              <span class="info-box-text">Transaksi</span>
              <span class="info-box-number" id="transaksi-card"></span>
            </div><!-- /.info-box-content -->
          </div><!-- /.info-box -->

        </div>
        
        <div class="col-sm-4" >
          <div class="info-box">
            <!-- Apply any bg-* class to to the icon to color it -->
            <span class="info-box-icon bg-green"><i class="fa fa-usd"></i></span>
            <div class="info-box-content">
              <span class="info-box-text">Penjualan</span>
              <span class="info-box-number" id="pendapatan-card">0</span>
            </div><!-- /.info-box-content -->
          </div><!-- /.info-box -->

        </div>
        
      </div>
      <div class="row"> <!-- chart 1 --> 

        <div class="col-md-12">
          <div class="box box-primary">
            <div class="box-header with-border">
              <h3 class="box-title">Penjualan Bulanan</h3>
            </div><!-- /.box-header -->
            <div class="box-body">
			  <div id="chart2"></div>
			  
            </div><!-- /.box-body -->
          </div><!-- /.box -->
        </div>
      </div>
      <div class="row">
        <div class="col-md-6">
          <div class="box box-success">
            <div class="box-header with-border">
              <h3 class="box-title">5 barang terlaris</h3>
            </div><!-- /.box-header -->
            <div class="box-body">
               <div id="chartPie1"></div>
              
            </div><!-- /.box-body -->
          </div><!-- /.box -->
        </div>
        <div class="col-md-6">
          <div class="box box-success">
            <div class="box-header with-border">
              <h3 class="box-title">5 jasa terlaris</h3>
            </div><!-- /.box-header -->
            <div class="box-body">
               <div id="chartPie2"></div>
              
            </div><!-- /.box-body -->
          </div><!-- /.box -->
        </div>
      </div>
      

	  
      <script src="https://code.highcharts.com/highcharts.js"></script>

    </section>
	  <script src="https://code.highcharts.com/modules/exporting.js"></script>
	  <!-- <script type="text/javascript" src="assets/js/chart1.js"></script> -->
	  <!-- <script type="text/javascript" src="assets/js/chart2.js"></script> -->
    <script type="text/javascript" src="<?=base_url()?>/assets/js/dataDashboard.js">
    
  </script>
  <script>
    $( document ).ready(function() {
      load_dash(<?=json_encode($this->input->get())?>);
    });
  </script>
    <!-- /.content -->
  <!-- /.content-wrapper -->
    <?php endif; ?>
  </div>
