   <!-- Content Wrapper. Contains page content -->
  <div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <section class="content-header">
      <h1>User</h1>
      <ol class="breadcrumb">
        <li><a><i class="fa fa-cogs"></i>Data Master</a></li>
        <li class="active">User</li>
      </ol>
    </section>

    <!-- Main content -->
    <section class="content">
      <div class="row">
        <div class="col-md-12">
          <div class="box  box-success">
            <div class="box-header">
              <h3 class="box-title">Data User</h3>
            </div>
            <!-- /.box-header -->
            <div class="box-body">
                <button class="btn btn-primary" id="tambah" data-toggle="modal" data-target="#myModal">Tambah</button>
                <table id="user-table" class="table table-bordered table-hover" data-id="<?=$this->router->fetch_class()?>">
                  <thead>
                  <tr>
                    <th width="40">No</th>
                    <th>Username</th>
                    <th>Nama</th>
                    <th>Group</th>
                    <th>Cabang</th>
                    <th>Alamat</th>
                    <th>Telp</th>
                    <th></th>
                  </tr>
                  </thead>
                  <tbody>
                    <?php $no=1; foreach ($data as $user) {?>
                      <tr>
                        <td><?=$no?></td>
                        <td><?=$user['_username']?></td>
                        <td><?=$user['_nama']?></td>
                        <td><?php 
                            if($user['_group']==1){echo "Owner";}
                            elseif($user['_group']==2){echo "Admin";}
                            elseif($user['_group']==3){echo "Kasir";}
                          ?></td>
                        <td><?=($user['_group'] == 3)?$user['_nama_cab']:""?></td>
                        <td><?=$user['_alamat']?></td>
                        <td><?=$user['_telp']?></td>
                        <td>
                          <button type="button" class="btn btn-warning btn-edit" data-id="<?=$user['_id']?>">Edit</button>
                          <button type="button" class="btn btn-danger btn-delete" data-id="<?=$user['_id']?>">Hapus</button>
                        </td>
                      </tr>
                      <?php $no++;} ?>
                  </tbody>
                </table>
              
            </div>
            <!-- /.box-body -->

          </div> 
        </div>
      </div>
 	  
	  </section>
    <!-- /.content -->
  </div>
  <!-- /.content-wrapper -->
  <div id="myModal" class="modal fade" role="dialog">
  <div class="modal-dialog">

    <!-- Modal content-->
    <form action="<?= base_url()."CUser/submit "?>" method="post">
      <div class="modal-content">
        <div class="modal-header">
          <button type="button" class="close" data-dismiss="modal">&times;</button>
          <h4 class="modal-title">User</h4>
        </div>
        <div class="modal-body">
          <?php 
          if(!empty(validation_errors()) ){
            echo "<div class='error-notif'>
            <span>".validation_errors()."
            </span>
          </div>";
          }
        ?>
          <input required="required" type="hidden" name="id">
          <div class="form-group">
            <label>Nama</label>
            <input required="required" type="text" value="<?php echo set_value('nama'); ?>" class="form-control" id="nama" name="nama">
          </div>
          <div class="form-group">
            <label>Username</label>
            <input required="required"  type="text" value="<?php echo set_value('username'); ?>" class="form-control" id="username" name="username">
          </div>
          <div class="form-group">
            <label>Password</label>
            <input required="required" type="password" value="<?php echo set_value('pass'); ?>" class="form-control" id="pass" name="pass">
          </div>
          <div class="form-group">
            <label>Telp</label>
            <input required="required" type="text" value="<?php echo set_value('telp'); ?>" class="form-control" id="telp" name="telp">
          </div>
          <div class="form-group">
            <label>Alamat</label>
            <textarea required="required" class="form-control" name="alamat"  id="alamat" cols="30" rows="10">
              <?php echo set_value('alamat'); ?>
            </textarea>
          </div>
          <div class="form-group">
            <label>Group</label>
            <select name="group" id="groupUser" class="form-control">
              <option value="1">Owner</option>
              <option value="2">Admin</option>
              <option value="3">Kasir</option>
            </select>
          </div>
          <div class="form-group" id="cbCabang" style="display: none;">
            <label>Cabang</label>
            <select name="id_cabang" id="cabang" class="form-control">
              <?php foreach ($cbCabang as $value) { ?>
                <option value="<?=$value->_id_cab?>"><?=$value->_nama_cab?></option>
              <?php } ?>
            </select>
          </div>
        </div>
        <div class="modal-footer">
          <button type="submit" class="btn btn-primary">Simpan</button>
          <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
        </div>
      </div>
    </form>

  </div>
</div>
  <script>
    $(function () {
      $('#user-table').DataTable({
        "paging": true,
        "lengthChange": false,
        "searching": false,
        "ordering": true,
        "info": true,
        "autoWidth": false
      });
    });
  </script>
  <script>
    $("#tambah").click(function() {
      $(".error-notif").remove();
      $(".form-group>#username").removeAttr("readonly");
    })
    $('.btn-edit').click(function() {
      console.log("asdasdasdsadas2");
      $(".form-group>#username").attr("readonly",true);
      $(".error-notif").remove();
    })
  </script>
  <?php  if (!empty(validation_errors()) ) :?>
  <script>
  $("#myModal").modal("show");
  </script>
<?php endif;?>