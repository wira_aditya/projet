   <!-- Content Wrapper. Contains page content -->
  <div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <section class="content-header">
      <h1>Peramalan</h1>
      <ol class="breadcrumb">
        <li><a><i class="fa fa-calendar"></i>Peramalan</a></li>
      </ol>
    </section>

    <!-- Main content -->
    <section class="content">
      <div class="row">
        <div class="col-md-12">
          <div class="box  box-success">
            <div class="box-header">
              <h3 class="box-title">Peramalan</h3>
            </div>
            <!-- /.box-header -->
            <div class="box-body">
              <form id="productTrans" role="form" class="form-inline" action="../peramalan/">
                <div class="form-group">
                  <input type="number" required="" value="<?=(!empty($src))?$src['tahun']:''?>" id="tahun" name="tahun" class="form-control" placeholder="Tahun">
                </div>
                <div class="form-group">
                  <select class="form-control" id="bulan" name="bulan">
                    <option value="1" <?=(isset($src)&&$src['bulan']== 1 ) ?"selected":""?> >Januari</option>
                    <option value="2" <?=(isset($src)&&$src['bulan']==2)?"selected":""?> >februari</option>
                    <option value="3" <?=(isset($src)&&$src['bulan']==3)?"selected":""?> >Maret</option>
                    <option value="4" <?=(isset($src)&&$src['bulan']==4)?"selected":""?> >April</option>
                    <option value="5" <?=(isset($src)&&$src['bulan']==5)?"selected":""?> >Mei</option>
                    <option value="6" <?=(isset($src)&&$src['bulan']==6)?"selected":""?> >Juni</option>
                    <option value="7" <?=(isset($src)&&$src['bulan']==7)?"selected":""?> >Juli</option>
                    <option value="8" <?=(isset($src)&&$src['bulan']==8)?"selected":""?> >Agustus</option>
                    <option value="9" <?=(isset($src)&&$src['bulan']==9)?"selected":""?> >September</option>
                    <option value="10" <?=(isset($src)&&$src['bulan']==10)?"selected":""?> >Oktober</option>
                    <option value="11" <?=(isset($src)&&$src['bulan']==11)?"selected":""?> >November</option>
                    <option value="12" <?=(isset($src)&&$src['bulan']==12)?"selected":""?> >Desember</option>
                  </select>
                </div>
    				   
                
                <div class="form-group">
    				      <button id="addTrBrg" class="btn btn-success btn-add" type="submit" >
                    <span class="fa fa-search"></span>
                  </button>
                </div>
                  
    				  </form>
              <table id="peramalan-table" class="table table-bordered table-hover" data-id="<?=$this->router->fetch_class()?>">
                <thead>
                <tr>
                  <th width="40">No</th>
                  <th>Cabang</th>
                  <th>Nama Barang</th>
                  <th>Stok Yang Disediakan</th>
                  <th>Perhitungan Mean Squared Error</th>
                  <th>Perhitungan Mean Absolute Percentage Error</th>
                  <th>Perhitungan Mean Absolute Deviation</th>
                </tr>
                </thead>
                <tbody id="tbdytrans">
                  <?php $no=1; 
                  if (!empty($data)) :
                    foreach ($data as $value) : ?>
                    <tr>
                      <td><?=$no++?></td>
                      <td><?=$value['_nama_cab']?></td>
                      <td><?=$value['_nama']?></td>
                      <td><?=$value['_ramalan']?></td>
                      <td><?=($value['_mse']<0)?str_replace("-", "(", $value['_mse']).")":$value['_mse']?></td>
                      <td><?=($value['_mape']<0)?str_replace("-", "(", $value['_mape']).")":$value['_mape']?>%</td>
                      <td><?=$value['_mad']?></td>
                    </tr>
                  <?php endforeach;
                    endif; ?>
                </tbody>
              </table>
              <div class="ket" style="margin-top: 50px">
                <p><strong>Mean Squared Error(MSE) : </strong> metode untuk mengevaluasi metode peramalan. Masing-masing kesalahan atau sisa dikuadratkan. Pendekatan ini mengatur kesalahan peramalan yang besar karena kesalahan-kesalahan itu dukuadratkan. Metode itu menghasilkan kesalahan kesalahan sedang yang  kemungkinan lebih baik untuk kesalahan kecil, tetapi kadang menghasilkan perbedaan yang besar</p>
                <p><strong>Mean Absolute Percentage Error(MAPE) : </strong>dihitung dengan menggunakan kesalahan absolut pada tiap periode dibagi dengan nilai observasi yang nyata untuk periode itu. Kemudian , merata-rata kesalahan persentase absolut tersebut. Pendekan ini berguna ketika ukuran atau besar variabel ramalan itu penting dalam mengevaluasi ketepatan ramalan. MAPE mengindikasi seberapa besar kesalahan dalam meramal yang dibandingakan dengan nilai nyata</p>
                <p><strong>Mean Absolute Deviation(MAD) : </strong>Metode untuk mengevaluasi metode peramalan menggunakan jumlah dari kesalahan-kesalahan yang absolut.Metode ini mengukur ketepatan ramalan dengan melakukan pengurangan nilai aktual dengan nilai hasil dari peramalan tersebut</p>
              </div>
            </div>
            <!-- /.box-body -->

          </div> 
        </div>
      </div>
 	  
	  </section>
    <!-- /.content -->
  </div>
  <!-- /.content-wrapper -->



  <script>
    $(function () {
      
      $('#peramalan-table').DataTable({
        "paging": true,
        "lengthChange": false,
        "searching": false,
        "ordering": false,
        "info": false,
        "autoWidth": true  
      });
        // var table = $('#produk-table').DataTable();
        
      
    });
  </script>