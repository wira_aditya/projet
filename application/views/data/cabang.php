   <!-- Content Wrapper. Contains page content -->
  <div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <section class="content-header">
      <h1>Cabang</h1>
      <ol class="breadcrumb">
        <li><a><i class="fa fa-cogs"></i>Data Master</a></li>
        <li class="active">Cabang</li>
      </ol>
    </section>

    <!-- Main content -->
    <section class="content">
      <div class="row">
        <div class="col-md-12">
          <div class="box  box-success">
            <div class="box-header">
              <h3 class="box-title">Data Cabang</h3>
            </div>
            <!-- /.box-header -->
            <div class="box-body">
                <button class="btn btn-primary" data-toggle="modal" data-target="#myModal">Tambah</button>
                <table id="user-table" class="table table-bordered table-hover" data-id="<?=$this->router->fetch_class()?>">
                  <thead>
                  <tr>
                    <th width="50">No</th>
                    <th>Nama cabang</th>
                    <th>Lokasi</th>
                    <th></th>
                  </tr>
                  </thead>
                  <tbody>
                    <?php $no=1; foreach ($data as $cabang) {?>
                      <tr>
                        <td><?=$no?></td>
                        <td><?=$cabang['_nama_cab']?></td>
                        <td><?=$cabang['_lokasi']?></td>
                        <td>
                          <button type="button" class="btn btn-warning btn-edit" data-id="<?=$cabang['_id_cab']?>">Edit</button>
                          <button type="button" class="btn btn-danger btn-delete" data-id="<?=$cabang['_id_cab']?>">Hapus</button>
                        </td>
                      </tr>
                      <?php $no++;} ?>
                  </tbody>
                </table>
              
            </div>
            <!-- /.box-body -->

          </div> 
        </div>
      </div>
 	  
	  </section>
    <!-- /.content -->
  </div>
  <!-- /.content-wrapper -->
  <div id="myModal" class="modal fade" role="dialog">
  <div class="modal-dialog">

    <!-- Modal content-->
    <form action="<?= base_url()."CCabang/submit "?>" method="post">
      <div class="modal-content">
        <div class="modal-header">
          <button type="button" class="close" data-dismiss="modal">&times;</button>
          <h4 class="modal-title">Cabang</h4>
        </div>
        <div class="modal-body">
          <input required="required" type="hidden" name="id_cab">
          <div class="form-group">
            <label>Nama Cabang</label>
            <input required="required" type="text" class="form-control" id="nama_cab" name="nama_cab">
          </div>
          <div class="form-group">
            <label>Lokasi</label>
            <textarea required="required" name="lokasi" id="lokasi" class="form-control" cols="30" rows="10"></textarea>
          </div>
        </div>
        <div class="modal-footer">
          <button type="submit" class="btn btn-primary">Simpan</button>
          <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
        </div>
      </div>
    </form>

  </div>
</div>
  <script>
    $(function () {
      $('#user-table').DataTable({
        "paging": true,
        "lengthChange": false,
        "searching": false,
        "ordering": false,
        "info": true,
        "autoWidth": false
      });
    });
  </script>