 <!-- Content Wrapper. Contains page content -->
  <div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <section class="content-header">
      <h1>Laporan Penjualan</h1>
      <ol class="breadcrumb">
        <li><a href="#"><i class="fa fa-print"></i>Laporan</a></li>
        <li class="active">Penjualan</li>
      </ol>
    </section>

    <!-- Main content -->
    <section class="content">
      <div class="row">
        <div class="col-md-12">
          <div class="box  box-success">
            <div class="box-header">
              <h3 class="box-title">Laporan Penjualan</h3>
            </div>
            <!-- /.box-header -->
            <div class="box-body">
              <form id="lapPenj-chart" style="width: 400px; margin: auto;" role="form" method="get"  class="form-inline" action="../laporan-penjualan/">
                  <div class="form-group">
                    <!-- <input class="form-control" id="tahun-lap-jual" name="tahun" value="<?=(!empty($src)&&!empty($src['tahun']))?$src['tahun']:''?>" type="number" placeholder="Tahun"> -->
                    
                    <select class="form-control" name="tahun" id="tahun-lap-jual">
                      <?php foreach ($tahun as $key => $value): ?>
                        <option value="<?=$value['tahun']?>" <?=($src['tahun'] == $value['tahun'])?"selected":""  ?>><?=$value['tahun']?></option>
                      <?php endforeach ?>
                    </select>
                  </div>
                  <div class="form-group">
                    <select class="form-control" name="bulan" id="bulan-lap-jual">
                        <option value="">Bulan</option>
                      <?php foreach ($bulan as $key => $value): ?>
                        <option value="<?=$key+1?>" <?=(!empty($src['bulan'])&& $src['bulan'] == $key+1)?"selected":""  ?>><?=$value?></option>
                      <?php endforeach ?>
                    </select>
                  </div>
                  
                  <div class="form-group">
                    <button id="addTrBrg" class="btn btn-success btn-add" type="submit" >
                      <span class="fa fa-search"></span>
                    </button>
                  </div>
                    
                </form>
              <div class="row">
                <div class="col-md-8 col-md-offset-2" id="barChartPenjualan" style="height: 500px"></div>
              </div>
              <div class="row">
                <div class="col-md-8 col-md-offset-2" id="barChartPenjualan2" style="height: 500px"></div>
              </div>
              <div class="row">
                <div class="col-md-8 col-md-offset-2" id="lineChartPenjualan" style="height: 500px"></div>
              </div>
              
              
            </div>
            <!-- /.box-body -->

          </div> 
        </div>
      </div>
      <div class="row">
        <div class="col-md-12">
          <div class="box  box-success">
            <div class="box-header">
              <h3 class="box-title">Laporan Penjualan</h3>
            </div>
            <!-- /.box-header -->
            <div class="box-body">
              
              <form id="lapPenj"  role="form" method="get"  class="form-inline" action="../laporan-penjualan/">
                <div class="form-group">
                  <input class="form-control" id="nama" name="nama" value="<?=(!empty($grid['nama']))?$grid['nama']:''?>" type="text" placeholder="Nama Barang">
                </div>
                <!-- cb jenis -->
                <div class="form-group">
                  <select class="form-control" name="jenis" id="jenis">
                      <option value="">Jenis</option>
                      <option value="1" <?=(!empty($grid['jenis'])&& $grid['jenis'] == 1)?"selected":""  ?>>Barang</option>
                      <option value="2" <?=(!empty($grid['jenis'])&& $grid['jenis'] == 2)?"selected":""  ?>>Jasa</option>
                  </select>
                </div>
                <!-- end cb jenis -->
                <!-- cb cabang -->
                <div class="form-group">
                  <select class="form-control" name="cabang" id="cabang">
                      <option value="">Cabang</option>
                    <?php foreach ($cabang_cb as $key => $value): ?>
                      <option value="<?=$value->_id_cab?>" <?=(!empty($grid['cabang'])&& $grid['cabang'] == $value->_id_cab)?"selected":""  ?>><?=$value->_nama_cab?></option>
                    <?php endforeach ?>
                  </select>
                </div>
                <div class="form-group">
                  <label for="dari">dari:</label>
                  <input type="date" id="dari" name="dari" value="<?=(!empty($grid['dari']))?$grid['dari']:''?>" class="form-control" placeholder="Tanggal Dari">
                </div>
                <div class="form-group">
                  <label for="sampai">sampai:</label>
                  <input type="date" id="sampai" name="sampai" value="<?=(!empty($grid['sampai']))?$grid['sampai']:''?>" class="form-control" placeholder="Tanggal Sampai">
                </div>
                <div class="form-group">
                  <button id="addTrBrg" class="btn btn-success btn-add" type="submit" >
                    <span class="fa fa-search"></span>
                  </button>
                </div>
                  
              </form>
              <div class="wrap-btn">
                <button type="button" id="cetakLapPenj" class="btn btn-lg btn-primary">Cetak</button>
                <button type="button" id="excelLapPenj" class="btn btn-lg btn-primary">Excel</button>
              </div>
              <table id="lappenjualan-table" class="table table-bordered table-hover" data-id="<?=$this->router->fetch_class()?>">
                <thead>
                <tr>
                  <th width="40">No</th>
                  <th>Kode Produk</th>
                  <th>Nama Produk</th>
                  <th>Jenis</th>
                  <th>Cabang</th>
                  <th>Stok In</th>
                  <th>Stok Out</th>
                  <th>Sisa</th>
                  <th>Harga Jual</th>
                  <th>Penjualan</th>
                </tr>
                </thead>
                <tbody>
                  <?php  
                    $no =0;
                    if (!empty($data)):
                    foreach ($data as $value) :
                    
                  ?>
                    <tr>
                      <td><?=++$no?></td>
                      <td><?=$value['_kode']?></td>
                      <td><?=$value['_nama']?></td>
                      <td><?=($value['_jenis']==1)?"Barang":"Jasa"?></td>
                      <td><?=$value['_nama_cab']?></td>
                      <td><?=$value['_stok']?></td>
                      <td><?=($value['_jenis'] == 2)?0:$value['qtyOut']?></td>
                      <td><?=($value['_jenis'] == 2)?0:$value['sisa']?></td>
                      <td><?=$value['_harga_jual']?></td>
                      <td><?=$value['totalJual']?></td>
                    </tr>
                  <?php endforeach;endif; ?>
                </tbody>
              </table>
            </div>
            <!-- /.box-body -->

          </div> 
        </div>
      </div>
    
    </section>
    <!-- /.content -->
  </div>
    <script src="https://code.highcharts.com/highcharts.js"></script>
  <script src="https://code.highcharts.com/modules/exporting.js"></script>
  <script type="text/javascript" src="../assets/js/chartPenjualan.js">
    
  </script>
  <script>
    $(function () {
      
      $('#lappenjualan-table').DataTable({
        "paging": true,
        "lengthChange": false,
        "searching": false,
        "ordering": false,
        "info": false,
        "autoWidth": true  
      });
        
      
    });
    $( document ).ready(function() {
      loadChart(<?=json_encode($this->input->get())?>);
    });
  </script>
  <!-- /.content-wrapper -->
 