 <!-- Content Wrapper. Contains page content -->
  <div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <section class="content-header">
      <h1>Laporan Stok</h1>
      <ol class="breadcrumb">
        <li><a href="#"><i class="fa fa-print"></i>Laporan</a></li>
        <li class="active">Stok</li>
      </ol>
    </section>
    <!-- Main content -->
    <section class="content">
      <div class="row">
        <div class="col-md-12">
          <div class="box  box-success">
            <div class="box-header">
              <h3 class="box-title">Laporan Stok</h3>
            </div>
            <!-- /.box-header -->
            <div class="box-body">
              <div class="row">
                <!-- <div class="col-md-8 col-md-offset-2" id="chartStok" style="height: 500px"></div> -->
              </div>
              <form id="lapStok" role="form" method="get"  class="form-inline" action="<?=base_url()?>laporan-stok/">
                <div class="form-group">
                  <input class="form-control" id="nama" name="nama" value="<?=(!empty($src))?$src['nama']:''?>" type="text" placeholder="Nama Barang">
                </div>
                <div class="form-group">
                  <select class="form-control" name="cabang" id="cabang">
                      <option value="">Cabang</option>
                    <?php foreach ($cabang_cb as $key => $value): ?>
                      <option value="<?=$value->_id_cab?>" <?=(!empty($src)&& $src['cabang'] == $value->_id_cab)?"selected":""  ?>><?=$value->_nama_cab?></option>
                    <?php endforeach ?>
                  </select>
                </div>
                <!-- <div class="form-group">
                  <input class="form-control" id="tgl" name="tgl" value="<?=(!empty($src))?$src['tgl']:''?>" type="date" >
                </div> -->
                <div class="form-group">
                  <button id="addTrBrg" class="btn btn-success btn-add" type="submit" >
                    <span class="fa fa-search"></span>
                  </button>
                </div>
                  
              </form>
              <div class="wrap-btn">
                <button type="button" id="cetakLapStok" class="btn btn-lg btn-primary">Cetak</button>
                <button type="button" id="excelLapStok" class="btn btn-lg btn-primary">Excel</button>
              </div>
              <table id="lapstok-table" class="table table-bordered table-hover" data-id="<?=$this->router->fetch_class()?>">
                <thead>
                <tr>
                  <th width="40">No</th>
                  <th>Kode</th>
                  <th>Nama Barang</th>
                  <th>Cabang</th>
                  <!-- <th>Stok Input</th>
                  <th>Stok Out</th>
                  <th>Sisa</th> -->
                  <th>Stok</th>
                </tr>
                </thead>
                <tbody>
                  <?php  
                    $no =0;
                    if (!empty($data)):
                    foreach ($data as $value) :
                    
                  ?>
                    <tr>
                      <td><?=++$no?></td>
                      <td><?=$value['_kode']?></td>
                      <td><?=$value['_nama']?></td>
                      <td><?=$value['_nama_cab']?></td>
                      <td><?=$value['sisa']?></td>
                    </tr>
                  <?php endforeach;endif; ?>
                </tbody>
              </table>
            </div>
            <!-- /.box-body -->

          </div> 
        </div>
      </div>
 	  
	  </section>
    <!-- /.content -->
  </div>
  <script src="https://code.highcharts.com/highcharts.js"></script>
  <script src="https://code.highcharts.com/modules/exporting.js"></script>
  <!-- <script type="text/javascript" src="../assets/js/chartStok.js"></script> -->
<script>
    $(function () {
      
      $('#lapstok-table').DataTable({
        "paging": true,
        "lengthChange": false,
        "searching": false,
        "ordering": false,
        "info": false,
        "autoWidth": true  
      });
        // var table = $('#produk-table').DataTable();
        
      
    });
  </script>

  <!-- /.content-wrapper -->
 