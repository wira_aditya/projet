   <!-- Content Wrapper. Contains page content -->
  <div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <section class="content-header">
      <h1>Transaksi</h1>
      <ol class="breadcrumb">
        <li><a><i class="fa fa-tag"></i>Transaksi</a></li>
      </ol>
    </section>

    <!-- Main content -->
    <section class="content">
      <div class="row">
        <div class="col-md-12">
          <div class="box  box-success">
            <div class="box-header">
              <h3 class="box-title">Transaksi</h3>
            </div>
            <!-- /.box-header -->
            <div class="box-body">
                <div class='error' id="error">
                  
                </div>
                <div class="row form-group">
                  <label class="col-sm-2" for="inputEmail1">No. Transaksi</label>
                  <div class="col-sm-5"><input type="text" value="<?= $noTr?>" readonly class="form-control" id="no_tr" placeholder="No. Transaksi"></div>
                </div>
              <form id="productTrans" role="form" class="form-inline">
              
                <input type="hidden" name="id_pro">
    				    <div class="form-group">
    			        <input class="form-control" id="kode" name="kode" type="text" placeholder="kode barang">
                </div>
                <div class="form-group">
                  <button data-toggle="modal" data-target="#modalBarang" class="btn btn-success btn-add" type="button">...</button>
                </div>
                <div class="form-group">
                  <input type="text" readonly="" name="nama" class="form-control" placeholder="Nama Barang">
    				    </div>
                <div class="form-group">
                  <input type="text" readonly="" name="harga_jual" class="form-control" placeholder="Harga">
                </div>  
                <div class="form-group">
                  <input type="number" id="qty" name="qty" class="form-control" placeholder="Qty">
                </div>
                <div class="form-group">
                  <input type="number" id="disk" name="disk" class="form-control" placeholder="Diskon">
                </div>
                <div class="form-group">
    				      <button disabled="" id="addTrBrg" class="btn btn-success btn-add" type="button" >
                    <span class="glyphicon glyphicon-plus"></span>
                  </button>
                </div>
                  
    				  </form>
                    <table id="user-table" class="table table-bordered table-hover" data-id="<?=$this->router->fetch_class()?>">
                      <thead>
                      <tr>
                        <th width="40">No</th>
                        <th>Kode Barang</th>
                        <th>Nama Barang</th>
                        <th>Qty</th>
                        <th>Harga</th>
                        <th>Disk</th>
                        <th>Total</th>
                        <th width="50"></th>
                      </tr>
                      </thead>
                      <tbody id="tbdytrans">
                        
                      </tbody>
                    </table>
                    <div class="col-xs-6">
                      <div class="text-total floatRight">
                          <label>Grand Total : </label>
                          <span class="floatRight" id="gt"></span>
                      </div>
                    </div>
                    <button data-toggle="modal" data-target="#modalBayar" class="btn btn-lg btn-primary floatRight"><i class="fa fa-shopping-cart" aria-hidden="true"></i> Bayar</button>
            </div>
            
            <!-- /.box-body -->

          </div> 
        </div>
      </div>
 	  
	  </section>
    <!-- /.content -->
  </div>
  <!-- /.content-wrapper -->

<div id="modalBarang" class="modal fade" role="dialog">
  <div class="modal-dialog modal-lg">

    <!-- Modal content-->
    <form action="<?= base_url()."CProduk/submit"?>" method="post">
      <div class="modal-content">
        <div class="modal-header">
          <button type="button" class="close" data-dismiss="modal">&times;</button>
          <h4 class="modal-title">Data Barang</h4>
        </div>
        <div class="modal-body">
          <table id="produk-table" class="table table-bordered table-hover" data-id="<?=$this->router->fetch_class()?>">
                  <thead>
                  <tr>
                    <th>No</th>
                    <th>Kode Barang</th>
                    <th>Nama Barang</th>
                    <th>Jenis</th>
                    <th>Stok</th>
                    <th>Harga jual</th>
                  </tr>
                  </thead>
                  <tbody id="BrgTransaksi">
                    <?php $no=1; foreach ($produk as $produk) {?>
                      <tr  data-id="<?=$produk['_id_pro']?>">
                        <td><?=$no?></td>
                        <td><?=$produk['_nama']?></td>
                        <td><?=$produk['_kode']?></td>
                        <td><?=($produk['_jenis'] == 1)?"Barang":"Jasa"?></td>
                        <td><?=$produk['_stok']?></td>
                        <td><?=$produk['_harga_jual']?></td>
                        
                      </tr>
                      <?php $no++;} ?>
                  </tbody>
                </table>
        </div>
        <div class="modal-footer">
          <button type="button" id="btnPilih" disabled="" class="btn btn-primary">Pilih</button>
        </div>
      </div>
    </form>

  </div>
</div>
<div id="modalBayar" class="modal fade" role="dialog">
  <div class="modal-dialog">

    <!-- Modal content-->
    <form id="fr-bayar" action="" method="post">
      <div class="modal-content">
        <div class="modal-header">
          <button type="button" class="close" data-dismiss="modal">&times;</button>
        </div>
        <div class="modal-body">
          <div class="form-group">
            <label>Grand Total</label>
            <input type="number" class="form-control" id="gtBayarTr" readonly name="grandTotal">
          </div>
          <div class="form-group">
            <label>Bayar</label>
            <input type="number" class="form-control" id="bayarTr" name="bayar">
          </div>
          <div class="form-group">
            <label>Kembali</label>
            <input type="number" class="form-control" id="kembaliTr" readonly name="kembali">
          </div>          
        </div>
        <div class="modal-footer">
          <button type="button" id="simpanBayar" class="btn btn-primary">Simpan</button>
        </div>
      </div>
    </form>

  </div>
</div>
  <script>
    $(function () {
      
      $('#produk-table').DataTable({
        "lengthChange": false,
        "searching": false,
        "ordering": false,
        "info": false,
        "autoWidth": false  
      });
      var table = $('#produk-table').DataTable();
      $('#produk-table tbody').on( 'click', 'tr', function () {
        // console.log("asdasdas");
          if ( $(this).hasClass('selected') ) {
              console.log("has");
              $(this).removeClass('selected');
              $("#btnPilih").attr("disabled","disabled");
          }
          else {
              table.$('tr.selected').removeClass('selected');
              console.log("no");
              $("#btnPilih").removeAttr("disabled");
              $(this).addClass('selected');
          }
      } );
      
    });
  </script>