 <!-- Content Wrapper. Contains page content -->
  <div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <section class="content-header">
      <h1>Laporan Peramalan</h1>
      <ol class="breadcrumb">
        <li><a href="#"><i class="fa fa-print"></i>Laporan</a></li>
        <li class="active">Peramalan</li>
      </ol>
    </section>

    <!-- Main content -->
    <section class="content">
      <div class="row">
        <div class="col-md-12">
          <div class="box  box-success">
            <div class="box-header">
              <h3 class="box-title">Laporan Peramalan</h3>
            </div>
            <!-- /.box-header -->
            <div class="box-body">
              <form id="lapRml" role="form" method="get"  class="form-inline" action="../laporan-peramalan/">
                <div class="form-group">
                  <input class="form-control" id="nama" name="nama" value="<?=(!empty($src))?$src['nama']:''?>" type="text" placeholder="Nama Barang">
                </div>
                <div class="form-group">
                  <select class="form-control" name="cabang" id="cabang">
                      <option value="">Cabang</option>
                    <?php foreach ($cabang_cb as $key => $value): ?>
                      <option value="<?=$value->_id_cab?>" <?=(!empty($src)&& $src['cabang'] == $value->_id_cab)?"selected":""  ?>><?=$value->_nama_cab?></option>
                    <?php endforeach ?>
                  </select>
                </div>
                <div class="form-group">
                  <input class="form-control" id="tahun" name="tahun" value="<?=(!empty($src))?$src['tahun']:''?>" type="number" placeholder="Tahun">
                </div>  
                <div class="form-group">
                  <select class="form-control" name="bulan" id="bulan">
                    <option value="">Bulan</option>
                    <?php foreach ($bln as $key => $value): ?>
                      <option value="<?=$key+1?>" <?=(!empty($src)&& $src['bulan'] == $key+1)?"selected":""  ?>><?=$value?></option>
                    <?php endforeach ?>
                  </select>
                </div>
                
                <div class="form-group">
                  <button id="addTrBrg" class="btn btn-success btn-add" type="submit" >
                    <span class="fa fa-search"></span>
                  </button>
                </div>
              </form>
              <div class="wrap-btn">
                <button type="button" id="cetakLapRml" class="btn btn-lg btn-primary">Cetak</button>
                <button type="button" id="excelLapRml" class="btn btn-lg btn-primary">Excel</button>
              </div>
              <table id="peramalan-table" class="table table-bordered table-hover" data-id="<?=$this->router->fetch_class()?>">
                <thead>
                <tr>
                  <th width="40">No</th>
                  <th>Kode Barang</th>
                  <th>Nama Barang</th>
                  <th>Cabang</th>
				          <th>Stok Yang Disediakan</th>
                  <th>Tahun</th>
                  <th>Bulan</th>
                  <th>Mean Squared Error</th>
                  <th>Mean Absolute Percentage Error</th>
                  <th>Mean Absolute Deviation</th>
                  <!-- <th>Mean Absolute Deviation</th> -->
                </tr>
                </thead>
                <tbody>         
                  <?php  
                    $no =0;
                    // print_r($data);die();
                    if (!empty($data)):
                    foreach ($data as $value) :
                    
                  ?>
                    <tr>
                      <td><?=++$no?></td>
                      <td><?=$value['_kode']?></td>
                      <td><?=$value['_nama']?></td>
                      <td><?=$value['_nama_cab']?></td>
					            <td><?=$value['_ramalan']?></td>
                      <td><?=$value['_thn']?></td>
                      <td><?=$bln[$value['_bln']-1]?></td>
                      <td><?=($value['_mse']<0)?str_replace("-", "(", $value['_mse']).")":$value['_mse']?></td>
                      <td><?=($value['_mape']<0)?str_replace("-", "(", $value['_mape']).")":$value['_mape']?>%</td>
                      <td><?=$value['_mad']?></td>
                    </tr>
                  <?php endforeach;endif; ?>
                </tbody>
              </table>

              <div class="ket" style="margin-top: 50px">
                <p><strong>Mean Squared Error(MSE) : </strong> metode untuk mengevaluasi metode peramalan. Masing-masing kesalahan atau sisa dikuadratkan. Pendekatan ini mengatur kesalahan peramalan yang besar karena kesalahan-kesalahan itu dukuadratkan. Metode itu menghasilkan kesalahan kesalahan sedang yang  kemungkinan lebih baik untuk kesalahan kecil, tetapi kadang menghasilkan perbedaan yang besar</p>
                <p><strong>Mean Absolute Percentage Error(MAPE) : </strong>dihitung dengan menggunakan kesalahan absolut pada tiap periode dibagi dengan nilai observasi yang nyata untuk periode itu. Kemudian , merata-rata kesalahan persentase absolut tersebut. Pendekan ini berguna ketika ukuran atau besar variabel ramalan itu penting dalam mengevaluasi ketepatan ramalan. MAPE mengindikasi seberapa besar kesalahan dalam meramal yang dibandingakan dengan nilai nyata</p>
                <p><strong>Mean Absolute Deviation(MAD) : </strong>Metode untuk mengevaluasi metode peramalan menggunakan jumlah dari kesalahan-kesalahan yang absolut.Metode ini mengukur ketepatan ramalan dengan melakukan pengurangan nilai aktual dengan nilai hasil dari peramalan tersebut</p>
              </div>
            </div>
            <!-- /.box-body -->

          </div> 
        </div>
      </div>
 	  
	  </section>
    <!-- /.content -->
  </div>
  <script>
    $(function () {
      $('#peramalan-table').DataTable({
        "paging": true,
        "lengthChange": false,
        "searching": false,
        "ordering": true,
        "info": true,
        "autoWidth": false
      });
    });
  </script>
  <!-- /.content-wrapper -->
 