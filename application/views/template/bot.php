
  <!-- Main Footer -->
  <footer class="main-footer">
    <!-- To the right -->
    <div class="pull-right hidden-xs">
      <!-- Anything you want -->
    </div>
    <!-- Default to the left -->
    <strong>Copyright &copy; 2016 <a href="#">VEGAS</a>.</strong> All rights reserved.
  </footer>

  <!-- Control Sidebar -->
</div>
<!-- ./wrapper -->

<!-- REQUIRED JS SCRIPTS -->

<!-- jQuery 2.2.3 -->
<!-- Bootstrap 3.3.6 -->
<!-- AdminLTE App -->
<script src="<?=base_url()?>assets/js/app.min.js"></script>
<!-- DATA TABLE -->
<script src="<?=base_url()?>assets/plugin/datatables/jquery.dataTables.min.js"></script>
<script src="<?=base_url()?>assets/plugin/datatables/dataTables.bootstrap.min.js"></script>
<!-- sweetalert -->
<script src="<?=base_url()?>assets/js/sweetalert.min.js"></script>
<!-- Optionally, you can add Slimscroll and FastClick plugins.
     Both of these plugins are recommended to enhance the
     user experience. Slimscroll is required when using the
     fixed layout. -->
<script src="<?=base_url()?>assets/js/script.js"></script>
</body>
</html>
