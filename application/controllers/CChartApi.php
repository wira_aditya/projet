<?php
header('Access-Control-Allow-Origin: *');
defined('BASEPATH') OR exit('No direct script access allowed');
	header('Content-Type: application/json');

class CChartApi extends CI_Controller {

	/**
	 * Index Page for this controller.
	 *
	 * Maps to the following URL
	 * 		http://example.com/index.php/welcome
	 *	- or -
	 * 		http://example.com/index.php/welcome/index
	 *	- or -
	 * Since this controller is set as the default controller in
	 * config/routes.php, it's displayed at http://example.com/
	 *
	 * So any other public methods not prefixed with an underscore will
	 * map to /index.php/welcome/<method_name>
	 * @see https://codeigniter.com/produk_guide/general/urls.html
	 */
	public $start_date;
	public $start_time;
	public $end_time;
	function __construct()
	{
        parent::__construct();
        if (!$this->session->has_userdata('id')) {
			redirect(base_url()."login",'refresh');
		}else{
			$this->load->model('Laporan_model');
			$this->bulan = ["Januari",
        						"Pebruari",
        						"Maret",
        						"April",
        						"Mei",
        						"Juni",
        						"Juli",
        						"Agustus",
        						"September",
        						"Oktober",
        						"November",
        						"Desember"
        					];
        	$this->month = date("m");
			$this->year = date("Y");
	
			
	
			
		}

	}
	public function stockChart()
	{
		$data = array();
		$data['cab'] = array();
		$data['data'] = array("in"=>array(),"out"=>array());
		// print_r($this->Laporan_model->barPenjualan());
		// die();
		$id= 0;
		$in = 0;
		$out = 0;
		$qr = $this->Laporan_model->getLapStok(array("order"=>1));
		foreach ($qr as $key => $value) {
			array_push($data['cab'], $value['_nama_cab']);
			if ($value['_id_cab'] == $id) {
				$in += $value['_stok'];
				$out += $value['qtyOut'];
			}elseif($id !=0 && $id != $value['_id_cab'] ){
				array_push($data['data']['in'], (int)$in);
				array_push($data['data']['out'], (int)$out);
				// print_r($in);
				$id = $value['_id_cab'];
				$in = $value['_stok'];
				$out = $value['qtyOut'];
			}else{
				$in = $value['_stok'];
				$out = $value['qtyOut'];
				$id = $value['_id_cab'];
			}
			if (count($qr) == $key+1) {
				array_push($data['data']['in'], (int)$in);
				array_push($data['data']['out'], (int)$out);
			}

		}
		print_r(json_encode($data));


	}
	public function penjualanBar()
	{
		$data = array();
		$data['cab'] = array();
		$data['data'] = array();
		$src = $this->input->get();
		$src['bulan'] = (empty($src['bulan'])) ? date("m"):$src['bulan'];
		$src['tahun'] = (empty($src['tahun'])) ? date("Y"):$src['tahun'];
		$data['bulan'] = $this->bulan[$src['bulan']-1];
		
		// print_r($this->Laporan_model->barPenjualan());
		// die();
		foreach ($this->Laporan_model->barPenjualan($src) as $key => $value) {
			
			array_push($data['cab'], $value['_nama_cab']);
			array_push($data['data'], (int)$value['total']);
		}
		print_r(json_encode($data));
	}
	public function penjualanBar2($value='')
	{
		$data = array();
		$data['cab'] = array();
		$data['data'] = array();
		$src = $this->input->get();
		$src['bulan'] = (empty($src['bulan'])) ? date("m"):$src['bulan'];
		$src['tahun'] = (empty($src['tahun'])) ? date("Y"):$src['tahun'];
		$data['bulan'] = $this->bulan[$src['bulan']-1];
		
		// print_r($this->Laporan_model->barPenjualan());
		// die();
		$src['jenis'] =1;
		foreach ($this->Laporan_model->barPenjualan2($src) as $key => $value) {
			
			array_push($data['cab'], $value['_nama_cab']);
			$data['data']['barang'][] = (int)$value['total'];
			// array_push($data['data']['barang'], (int)$value['total']);
		}
		$src['jenis'] =2;
		foreach ($this->Laporan_model->barPenjualan2($src) as $key => $value) {
			
			// array_push($data['cab'], $value['_nama_cab']);
			$data['data']['jasa'][] = (int)$value['total'];
			// array_push($data['data']['barang'], (int)$value['total']);
		}
		print_r(json_encode($data));
	}
	public function penjualanLine()
	{
		$src = $this->input->get();
	
		if (empty($src['bulan'])) {
			$src['bulan'] = date("m");
		}
		if (empty($src['tahun'])) {
			$tahun = $this->Laporan_model->getTahun();
			$thn = (!empty($tahun)) ? max($tahun)['tahun']:date("Y");
			$src['tahun'] = $thn;
		}
		$this->month = $src['bulan'];
		$this->year = $src['tahun'];

		$this->start_date = "01-".$this->month."-".$this->year;
		$this->start_time = strtotime($this->start_date);
		// print_r($this->month);
		// die();
		$this->end_time = strtotime("+1 month", $this->start_time);
		for($i=$this->start_time; $i<$this->end_time; $i+=86400)
			{
			   $this->list[] = date('d', $i);
			}
		
		$data['bulan'] = $this->bulan[$src['bulan']-1];
		$data['tgl'] = $this->list;
		$x = $this->Laporan_model->getLapPenjualan("",array("chart"=>true,"src"=>$src));
		foreach ($this->list as $key => $value) {
			if (empty($x)) {
				$data['data'][$key] =   0;
			}
			foreach ($x as $key2 => $value2) {
				if ($value  == $value2['bulan']) {
					$data['data'][$key] =   (int)$value2['jumlah'] ;
					break;
				}else{
					$data['data'][$key] =   0;
					
				}
			}
		}
			// echo "$key</br>";
		print_r(json_encode($data));
	}


	public function dashboard()
	{
		$src = $this->input->get();
		// print_r($src);
		if (empty($src['bulan'])) {
			$src['bulan'] = date("m");
		}
		if (empty($src['tahun'])) {
			$tahun = $this->Laporan_model->getTahun();
			$thn = (!empty($tahun)) ? max($tahun)['tahun']:date("Y");
			$src['tahun'] = $thn;
		}
		if (!empty($src) && !empty($src['bulan'])) {
			$this->month = $src['bulan'];
		}
		if (!empty($src) && !empty($src['tahun'])) {
			$this->year=$src['tahun'];
		}
		$this->start_date = "01-".$this->month."-".$this->year;
			$this->start_time = strtotime($this->start_date);
	
			$this->end_time = strtotime("+1 month", $this->start_time);
		for($i=$this->start_time; $i<$this->end_time; $i+=86400)
			{
			   $this->list[] = date('d', $i);
			}
			$srcDashboard = array("tahun"=>$src['tahun']);
		$data['bulan'] = $this->bulan[(int)$this->month-1];
		$barang = $this->Laporan_model->getLapPenjualan("",array("dashboard"=>true,"chart"=>true,"jenis"=>1,"src"=>$srcDashboard));
		// print_r($barang);
		// die();
		$jasa = $this->Laporan_model->getLapPenjualan("",array("dashboard"=>true,"chart"=>true,"jenis"=>2,"src"=>$srcDashboard));
		// print_r($jasa);
		// die();
		$data['bar']['bulan'] = $this->bulan;
		foreach ($this->bulan as $key => $value) {
			if (empty($barang)) {
					$data['bar']['data']["barang"][$key] =   0;
			}else{
				foreach ($barang as $key2 => $value2) {
					if ($key+1  == $value2['bulan']) {
						$data['bar']['data']["barang"][$key] =  +(int)$value2['jumlah'] ;
						break;
					}else{
						$data['bar']['data']["barang"][$key] =   0;
						
					}
				}
			}if (empty($jasa)) {
					$data['bar']['data']["jasa"][$key] =   0;
			}else{
				foreach ($jasa as $key2 => $value2) {
					if ($key+1  == $value2['bulan']) {
						$data['bar']['data']["jasa"][$key] =   (int)$value2['jumlah'] ;
						break;
					}else{
						$data['bar']['data']["jasa"][$key] =   0;
						
					}
				}
			}
		}
		$jenis = 1;
		$pie = $this->Laporan_model->pie_dash($src,$jenis);
		// if (empty($pie)) {
		// 	$data['pie']['data'][0]['name'] = "";
		// 	$data['pie']['data'][0]['y'] = 0;
		// }
		$data['pieBarang']['data'] = array();
		foreach ($pie as $key => $value) {
			$data['pieBarang']['data'][$key]['name'] = $value['_nama']."(".$value['_nama_cab'].")";
			$data['pieBarang']['data'][$key]['y'] = (int)$value['jumlah'];
		}
		$jenis = 2;
		$pie = $this->Laporan_model->pie_dash($src,$jenis);

		$data['pieJasa']['data'] = array();
		foreach ($pie as $key => $value) {
			$data['pieJasa']['data'][$key]['name'] = $value['_nama']."(".$value['_nama_cab'].")";
			$data['pieJasa']['data'][$key]['y'] = (int)$value['jumlah'];
		}
		$data['card'] = $this->Laporan_model->card($src)[0];
		print_r(json_encode($data));
	}
		
}