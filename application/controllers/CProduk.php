<?php
header('Access-Control-Allow-Origin: *');
defined('BASEPATH') OR exit('No direct script access allowed');

class CProduk extends CI_Controller {

	/**
	 * Index Page for this controller.
	 *
	 * Maps to the following URL
	 * 		http://example.com/index.php/welcome
	 *	- or -
	 * 		http://example.com/index.php/welcome/index
	 *	- or -
	 * Since this controller is set as the default controller in
	 * config/routes.php, it's displayed at http://example.com/
	 *
	 * So any other public methods not prefixed with an underscore will
	 * map to /index.php/welcome/<method_name>
	 * @see https://codeigniter.com/produk_guide/general/urls.html
	 */
	function __construct()
	{
        parent::__construct();
        if (!$this->session->has_userdata('id')) {
			redirect(base_url()."login",'refresh');
		}
		// $this->load->helper(array('form', 'url'));
		$this->load->library('form_validation');


	}
	public function index()
	{
		$this->load->model('Produk_model');
		$this->load->model('Cabang_model');

		$src =  $this->input->get();

        $data['data'] = $this->Produk_model->getUser("",$src);
		$data['src'] = $src;
        $data['cbCabang'] = $this->Cabang_model->cbCabang();
        $menu['menu'] = 'produk';
		$this->load->view('template/top',$menu);
		$this->load->view('data/produk',$data);
		$this->load->view('template/bot');

	}
	public function edit()
	{
		// return ajax for form data on edit button
		$id = $this->input->get('id');
		$this->load->model('Produk_model');
		$data = $this->Produk_model->edit($id);
		echo json_encode($data);
	}
	public function submit()
	{
		$this->load->model('Produk_model');
		$data = array(
	        '_nama' => $this->input->post('nama'),
	        '_kode' => $this->input->post('kode'),
	        '_jenis' => $this->input->post('jenis'),
	        '_id_cab' => $this->input->post('id_cab'),
	        '_stok' => $this->input->post('stok'),
	        '_harga_pokok' => $this->input->post('harga_pokok'),
	        '_harga_jual' => $this->input->post('harga_jual'),
	        '_status' => 1
         );
		$uniq='';
		// die();
		if (empty($this->input->post('id_pro')) && !is_numeric($this->input->post('id_pro'))) {
	        $this->form_validation->set_rules('kode', 'Kode Produk','is_unique[produk_._kode]',
	        									array('is_unique' => '%s. sudah tersedia'));
	        if ($this->form_validation->run() == FALSE)
	        {
	                $this->index();
	        }else{
				$data = $this->Produk_model->insert($data);
				if ($data) {
					redirect(base_url()."produk/",'refresh');
				}
	        	
	        }
		// print_r(empty($this->input->post('id_pro')) && !is_numeric($this->input->post('id_pro')));

		}
        else
        {
		        $data['_id_pro'] = $this->input->post('id_pro');
				$data = $this->Produk_model->update($data);
			if ($data) {
				redirect(base_url()."produk/",'refresh');
			}
        }									
	}
	public function delete()
	{
		$id = array('_id_pro'=>$this->input->get('id'));
		$this->load->model('Produk_model');
		echo $this->Produk_model->delete($id);
		
	}
}
