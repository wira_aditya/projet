<?php
header('Access-Control-Allow-Origin: *');
defined('BASEPATH') OR exit('No direct script access allowed');

class CCabang extends CI_Controller {

	/**
	 * Index Page for this controller.
	 *
	 * Maps to the following URL
	 * 		http://example.com/index.php/welcome
	 *	- or -
	 * 		http://example.com/index.php/welcome/index
	 *	- or -
	 * Since this controller is set as the default controller in
	 * config/routes.php, it's displayed at http://example.com/
	 *
	 * So any other public methods not prefixed with an underscore will
	 * map to /index.php/welcome/<method_name>
	 * @see https://codeigniter.com/produk_guide/general/urls.html
	 */
	function __construct()
	{
        parent::__construct();
        if (!$this->session->has_userdata('id')) {
			redirect(base_url()."login",'refresh');
		}

	}
	public function index()
	{
		$this->load->model('Cabang_model');
        $data['data'] = $this->Cabang_model->getCabang();
        $menu['menu'] = 'cabang';
		$this->load->view('template/top',$menu);
		$this->load->view('data/cabang',$data);
		$this->load->view('template/bot');
	}
	public function edit()
	{
		// return ajax for form data on edit button
		$id = $this->input->get('id');
		$this->load->model('Cabang_model');
		$data = $this->Cabang_model->edit($id);
		echo json_encode($data);
	}
	public function submit()
	{
		$this->load->model('Cabang_model');
		$data = array(
	        '_nama_cab' => $this->input->post('nama_cab'),
	        '_lokasi' => $this->input->post('lokasi'),
         );
		if (!empty($this->input->post('id_cab')) && is_numeric($this->input->post('id_cab'))) {
	        $data['_id_cab'] = $this->input->post('id_cab');
			$data = $this->Cabang_model->update($data);
		}else{
			$data = $this->Cabang_model->insert($data);
		}
		if ($data) {
			redirect(base_url()."cabang/",'refresh');
		}
	}
	public function delete()
	{
		// $id = array('_id_pro'=>$this->input->get('id'));
		// $this->load->model('produk_model');
		// echo $this->produk_model->delete($id);
		
	}
}
