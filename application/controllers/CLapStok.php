<?php
header('Access-Control-Allow-Origin: *');
defined('BASEPATH') OR exit('No direct script access allowed');

class CLapStok extends CI_Controller {

	/**
	 * Index Page for this controller.
	 *
	 * Maps to the following URL
	 * 		http://example.com/index.php/welcome
	 *	- or -
	 * 		http://example.com/index.php/welcome/index
	 *	- or -
	 * Since this controller is set as the default controller in
	 * config/routes.php, it's displayed at http://example.com/
	 *
	 * So any other public methods not prefixed with an underscore will
	 * map to /index.php/welcome/<method_name>
	 * @see https://codeigniter.com/produk_guide/general/urls.html
	 */
	function __construct()
	{
        parent::__construct();
        if (!$this->session->has_userdata('id')) {
			redirect(base_url()."login",'refresh');
		}else{
			$this->load->model('Laporan_model');
		}

	}
	public function index()
	{	
        $menu['menu'] = 'lpStok';
		$this->load->view('template/top',$menu);
		$data =  $this->input->get();
		$rec['data'] = $this->Laporan_model->getLapStok($data);
		$rec['src'] = $data;
		$this->load->model('Cabang_model');
        $rec['cabang_cb'] = $this->Cabang_model->cbCabang();
        if ($rec['cabang_cb'][0]->_id_cab == 0) {
        	$rec['cabang_cb'][0]->_nama_cab = "Cabang";
        }
		$this->load->view('data/lapStok',$rec);
		$this->load->view('template/bot');
	}
	public function cetak()
	{
		$data =  $this->input->get();
		$rec['data'] = $this->Laporan_model->getLapStok($data);
		$rec['cabang'] = (!empty($data['cabang'])&&!empty($rec['data'])) ? $rec['data'][0]['_nama_cab'] : "";
		$this->load->view('data/laporan/cetakLapStok',$rec);
	}
	public function excel()
	{
		$data =  $this->input->get();
		$rec['data'] = $this->Laporan_model->getLapStok($data);
		$rec['cabang'] = (!empty($data['cabang'])&&!empty($rec['data'])) ? $rec['data'][0]['_nama_cab'] : "";
		$this->load->view('data/laporan/excelLapStok',$rec);

	}
}
