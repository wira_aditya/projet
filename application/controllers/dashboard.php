<?php
header('Access-Control-Allow-Origin: *');
defined('BASEPATH') OR exit('No direct script access allowed');

class Dashboard extends CI_Controller {
	/**
	 * Index Page for this controller.
	 *
	 * Maps to the following URL
	 * 		http://example.com/index.php/welcome
	 *	- or -
	 * 		http://example.com/index.php/welcome/index
	 *	- or -
	 * Since this controller is set as the default controller in
	 * config/routes.php, it's displayed at http://example.com/
	 *
	 * So any other public methods not prefixed with an underscore will
	 * map to /index.php/welcome/<method_name>
	 * @see https://codeigniter.com/user_guide/general/urls.html
	 */

	function __construct()
	{
        parent::__construct();
        if (!$this->session->has_userdata('id')) {
			redirect(base_url()."login",'refresh');
		}else if ($this->session->group ==3) {
			redirect(base_url()."transaksi",'refresh');
		}

	}
	public function index()
	{
		$this->load->model('Laporan_model');
		$tahun = $this->Laporan_model->getTahun();
		$tahun = (empty($tahun)) ? date("Y") : $tahun;
		// print_r(($this->session->group));
		$menu['menu'] = 'dashboard';
		$this->load->view('template/top',$menu);
		if (empty($this->input->get()['tahun'])) {
			$data['src']['tahun'] =(!empty($tahun)) ?  max($tahun)['tahun'] : date("Y");
		}else{
			$data['src']['tahun'] = $this->input->get()['tahun'];
		}
		if (empty($this->input->get()['bulan'])) {
			$data['src']['bulan'] = (int)date("m");
		}else{
			$data['src']['bulan'] = $this->input->get()['bulan'];
		}
		$data['bln'] = ["Januari",
        						"Pebruari",
        						"Maret",
        						"April",
        						"Mei",
        						"Juni",
        						"Juli",
        						"Agustus",
        						"September",
        						"Oktober",
        						"November",
        						"Desember"
        					];
        $data['tahun'] = $tahun;
		$this->load->view('data/dashboard',$data);

		$this->load->view('template/bot');
	}
	public function redirecting()
	{
		if ($this->session->group == 1) {
			redirect(base_url()."login",'refresh');
		}
	}
}
