<?php
header('Access-Control-Allow-Origin: *');
defined('BASEPATH') OR exit('No direct script access allowed');

class CPeramalan extends CI_Controller {

	/**
	 * Index Page for this controller.
	 *
	 * Maps to the following URL
	 * 		http://example.com/index.php/welcome
	 *	- or -
	 * 		http://example.com/index.php/welcome/index
	 *	- or -
	 * Since this controller is set as the default controller in
	 * config/routes.php, it's displayed at http://example.com/
	 *
	 * So any other public methods not prefixed with an underscore will
	 * map to /index.php/welcome/<method_name>
	 * @see https://codeigniter.com/produk_guide/general/urls.html
	 */
	function __construct()
	{
        parent::__construct();
        if (!$this->session->has_userdata('id')) {
			redirect(base_url()."login",'refresh');
		}


	}
	public function index()
	{
		$this->load->model('peramalan_model');

		$src =  $this->input->get();
		$data = array();
		if (!empty($src)) {
			# code...
	        // $data['data'] = $this->Produk_model->getUser("",$src);
			$data['src'] = $src;
			$bln = $src['bulan'];
			$thn = $src['tahun'];
			$rm = $this->peramalan_model->peramalan($thn,$bln);
			$data['data'] =($rm) ? $this->peramalan_model->getPeramalan($thn,$bln) : "";
			// print_r($data);
			// die();
	        // $data['data'] = $this->cabang_model->getCabang();
	        // die();
		}
        $menu['menu'] = 'peramalan';
		$this->load->view('template/top',$menu);
		$this->load->view('data/peramalan',$data);
		$this->load->view('template/bot');
	}
	
}
