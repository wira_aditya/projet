<?php
header('Access-Control-Allow-Origin: *');
defined('BASEPATH') OR exit('No direct script access allowed');

class CLogin extends CI_Controller {

	/**
	 * Index Page for this controller.
	 *
	 * Maps to the following URL
	 * 		http://example.com/index.php/welcome
	 *	- or -
	 * 		http://example.com/index.php/welcome/index
	 *	- or -
	 * Since this controller is set as the default controller in
	 * config/routes.php, it's displayed at http://example.com/
	 *
	 * So any other public methods not prefixed with an underscore will
	 * map to /index.php/welcome/<method_name>
	 * @see https://codeigniter.com/produk_guide/general/urls.html
	 */
	public function index()
	{
		$this->load->view('data/login');
	}
	public function loginCheck()
	{
		$this->load->model('User_model');
		$data = array(
	        '_username' => $this->input->post('un'),
	        '_pass' => $this->input->post('pw')
        );
		$data = $this->User_model->loginValidate($data);
		if (!count($data)) {
			$this->session->set_flashdata('error', 'Mohon maaf. User/Password yang anda masukan salah.');
		}else{
			// print_r($data->_username);
			$sessiondata = array(
			        'nama'  => $data->_nama,
			        'id'     => $data->_id,
			        'group' => $data->_group,
			        'cabang' => $data->_id_cabang
			);
			$this->session->set_userdata($sessiondata);
			// print_r($this->session->userdata());
		}
			redirect(base_url()."login/",'refresh');

	}
	public function logout()
	{
		$session = array("username","id","group","cabang");
		$this->session->unset_userdata($session);
		redirect(base_url()."login/",'refresh');
	}
	
	// public function edit()
	// {
	// 	// return ajax for form data on edit button
	// 	$id = $this->input->get('id');
	// 	$this->load->model('cabang_model');
	// 	$data = $this->cabang_model->edit($id);
	// 	echo json_encode($data);
	// }
	// public function submit()
	// {
	// 	$this->load->model('cabang_model');
	// 	$data = array(
	//         '_nama_cab' => $this->input->post('nama_cab'),
	//         '_lokasi' => $this->input->post('lokasi'),
 //         );
	// 	if (!empty($this->input->post('id_cab')) && is_numeric($this->input->post('id_cab'))) {
	//         $data['_id_cab'] = $this->input->post('id_cab');
	// 		$data = $this->cabang_model->update($data);
	// 	}else{
	// 		$data = $this->cabang_model->insert($data);
	// 	}
	// 	if ($data) {
	// 		redirect(base_url()."cabang/",'refresh');
	// 	}
	// }
	// public function delete()
	// {
	// 	// $id = array('_id_pro'=>$this->input->get('id'));
	// 	// $this->load->model('produk_model');
	// 	// echo $this->produk_model->delete($id);
		
	// }
}
