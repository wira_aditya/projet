<?php
header('Access-Control-Allow-Origin: *');
defined('BASEPATH') OR exit('No direct script access allowed');

class CTrans extends CI_Controller {

	/**
	 * Index Page for this controller.
	 *
	 * Maps to the following URL
	 * 		http://example.com/index.php/welcome
	 *	- or -
	 * 		http://example.com/index.php/welcome/index
	 *	- or -
	 * Since this controller is set as the default controller in
	 * config/routes.php, it's displayed at http://example.com/
	 *
	 * So any other public methods not prefixed with an underscore will
	 * map to /index.php/welcome/<method_name>
	 * @see https://codeigniter.com/produk_guide/general/urls.html
	 */
	function __construct()
	{
        parent::__construct();
        if (!$this->session->has_userdata('id')) {
			redirect(base_url()."login",'refresh');
		}

	}
	public function index()
	{
		$this->load->model('Produk_model');
        $menu['menu'] = 'transaksi';
        $data['noTr'] = date('Ymdhis');
        $data['produk'] = $this->Produk_model->getUser($this->session->cabang,"");
        $this->delete();
		$this->load->view('template/top',$menu);
		$this->load->view('data/transaksi',$data);
		$this->load->view('template/bot');
	}
	public function barangload()
	{
		$where=array();
		$where['id'] = $this->input->get('id');
		$where['kode'] = $this->input->get('kode');
		$this->load->model('produk_model');
		$data = $this->produk_model->getRowProduct($where);
		$data = (empty($data)) ? 0 : $data;
		echo json_encode($data);
	}
	public function addTr($value='')
	{
		$this->load->model('tr_model');
		$disk = (!empty($this->input->post('disk'))) ? $this->input->post('disk') : 0;
		$total = ($this->input->post('harga_jual') * $this->input->post('qty'))*((100-$disk)/100);
		$x = array("_user"=>$this->session->id,"_id_pro"=>$this->input->post('id_pro'));
		$edit = $this->tr_model->getRowTmp($x);
		if (empty($edit)) {
			$data = array(
		        '_id_pro' => $this->input->post('id_pro'),
		        '_harga' => $this->input->post('harga_jual'),
		        '_qty' => $this->input->post('qty'),
		        '_disk' => $disk,
		        '_total' => $total,
		        '_user' =>$this->session->id
	         );
			$data = $this->tr_model->insertTmp($data);
			# code...
 		}else{
			$qty = $edit->_qty+$this->input->post('qty');
			$total = ($this->input->post('harga_jual') * $qty)*((100-$disk)/100);

			$data=array(
				'_qty' => $qty,
				'_total' =>$total,
		        '_disk' => $disk,
			    '_harga' => $this->input->post('harga_jual'),
		        '_id_pro' => $this->input->post('id_pro'),
		        '_user' =>$this->session->id,
		        '_id_detail' => $edit->_id_detail
			);
			$data = $this->tr_model->updateTmp($data);
		}
		// $success=($data)?true:false;
		echo json_encode($data); 
		
	}
	public function delete()
	{
		$this->load->model('tr_model');
		if ($this->input->get('id')) {
			$data = array("_id_detail"=>$this->input->get('id'),
							"_user"=>$this->session->id);
			$data = $this->tr_model->deleteRowTmp($data);
			$success=($data)?true:false;
			$data = $this->tr_model->getTemp();
			echo json_encode(array("success"=>$success,"data"=>$data));
		}else{
			$data = $this->tr_model->deleteTmp();
		}
	}
	public function submit()
	{
		$this->load->model('tr_model');
		$x=$this->tr_model->getQty();
		// print_r($x->qty);
		$data = array(
	        '_no_tr' => $this->input->post('notr'),
	        '_id_user' => $this->session->id,
	        '_qty' => $x->qty,
	        '_total' => $this->input->post('grandTotal'),
	        '_tgl' => date("Y-m-d")
         );

		$data = $this->tr_model->insert($data);
		if ($data) {
			print_r(json_encode(array("success"=>true,"text"=>"insert")));
		}
	}
	public function cetak()
	{
		$this->load->model('tr_model');

		$data =  $this->input->get();
        $rec['header'] = $data;
        $rec['det'] = $this->tr_model->getData($data);
		$rec['cabang'] = (!empty($data['cabang'])&&!empty($rec['data'])) ? $rec['data'][0]['_nama_cab'] : "";

		$this->load->view('data/laporan/cetak',$rec);

	}	
}
