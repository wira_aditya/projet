<?php
header('Access-Control-Allow-Origin: *');
defined('BASEPATH') OR exit('No direct script access allowed');

class CLapPeramalan extends CI_Controller {

	/**
	 * Index Page for this controller.
	 *
	 * Maps to the following URL
	 * 		http://example.com/index.php/welcome
	 *	- or -
	 * 		http://example.com/index.php/welcome/index
	 *	- or -
	 * Since this controller is set as the default controller in
	 * config/routes.php, it's displayed at http://example.com/
	 *
	 * So any other public methods not prefixed with an underscore will
	 * map to /index.php/welcome/<method_name>
	 * @see https://codeigniter.com/produk_guide/general/urls.html
	 */
	var $data = array();
	function __construct()
	{
        parent::__construct();
        if (!$this->session->has_userdata('id')) {
			redirect(base_url()."login",'refresh');
		}
			$this->load->model('Laporan_model');

        	$this->data['bln'] = ["Januari",
        						"Pebruari",
        						"Maret",
        						"April",
        						"Mei",
        						"Juni",
        						"Juli",
        						"Agustus",
        						"September",
        						"Oktober",
        						"November",
        						"Desember"
        					];
	}
	public function index()
	{
		// die();
        $menu['menu'] = 'lpPeramalan';
        $src = $this->input->get();
        $this->data['src'] = $src;
        $this->data['data'] = $this->Laporan_model->getLapPeramalan($src);
        $this->load->model('Cabang_model');
        $this->data['cabang_cb'] = $this->Cabang_model->cbCabang();
        if ($this->data['cabang_cb'][0]->_id_cab == 0) {
        	$this->data['cabang_cb'][0]->_nama_cab = "Cabang";
        }
		// print_r($this->data);
        // print_r($data);
		$this->load->view('template/top',$menu);
		$this->load->view('data/lapPeramalan',$this->data);
		$this->load->view('template/bot');
	}
	public function cetak()
	{
		$src =  $this->input->get();
		$this->data['data'] = $this->Laporan_model->getLapPeramalan($src);
		$this->data['cabang'] = (!empty($this->data['data'])&&!empty($src['cabang'])) ? $this->data['data'][0]['_nama_cab'] : "";
		$this->load->view('data/laporan/cetakLapRamal',$this->data);
	}
	public function excel()
	{
		$src =  $this->input->get();
		$this->data['data'] = $this->Laporan_model->getLapPeramalan($src);
		$this->data['cabang'] = (!empty($this->data['data'])&&!empty($src['cabang'])) ? $this->data['data'][0]['_nama_cab'] : "";
		$this->load->view('data/laporan/excelLapRamal',$this->data);

	}
}
