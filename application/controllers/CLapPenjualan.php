<?php
header('Access-Control-Allow-Origin: *');
defined('BASEPATH') OR exit('No direct script access allowed');

class CLapPenjualan extends CI_Controller {

	/**
	 * Index Page for this controller.
	 *
	 * Maps to the following URL
	 * 		http://example.com/index.php/welcome
	 *	- or -
	 * 		http://example.com/index.php/welcome/index
	 *	- or -
	 * Since this controller is set as the default controller in
	 * config/routes.php, it's displayed at http://example.com/
	 *
	 * So any other public methods not prefixed with an underscore will
	 * map to /index.php/welcome/<method_name>
	 * @see https://codeigniter.com/produk_guide/general/urls.html
	 */
	function __construct()
	{
        parent::__construct();
        if (!$this->session->has_userdata('id')) {
			redirect(base_url()."login",'refresh');
		}else{
			$this->load->model('Laporan_model');
		}

	}
	public function index()
	{	

		$tahun = $this->Laporan_model->getTahun();
		$tahun = (empty($tahun)) ? date("Y") : $tahun;
		if (empty($this->input->get()['tahun'])) {
			$data['src']['tahun'] = (!empty($tahun)) ? max($tahun)['tahun'] : date('Y');
		}else{
			$data['src']['tahun'] = $this->input->get()['tahun'];
		}
		if (empty($this->input->get()['bulan'])) {
			$data['src']['bulan'] = (int)date("m");
		}else{
			$data['src']['bulan'] = $this->input->get()['bulan'];
		}
		$data['bulan'] = ["Januari",
        					 	"Pebruari",
        						"Maret",
        						"April",
        						"Mei",
        						"Juni",
        						"Juli",
        						"Agustus",
        						"September",
        						"Oktober",
        						"November",
        						"Desember"
        					];
        $menu['menu'] = 'lpPenjualan';
        $src = $this->input->get();
        $data['grid'] = $src;
        $this->load->model('Cabang_model');
        $data['cabang_cb'] = $this->Cabang_model->cbCabang();
        if ($data['cabang_cb'][0]->_id_cab == 0) {
        	$data['cabang_cb'][0]->_nama_cab = "Cabang";
        }
        $data['data'] = $this->Laporan_model->getLapPenjualanGrid($src);
        $data['tahun'] = $tahun;
        // print_r($data);
        // die();
		$this->load->view('template/top',$menu);
		$this->load->view('data/lapPenjualan',$data);
		$this->load->view('template/bot');
	}
	public function cetak()
	{
		$data =  $this->input->get();
        $rec['data'] = $this->Laporan_model->getLapPenjualanGrid($data);
		$rec['cabang'] = (!empty($data['cabang'])&&!empty($rec['data'])) ? $rec['data'][0]['_nama_cab'] : "";

		$this->load->view('data/laporan/cetakLapPenj',$rec);

	}
	public function excel()
	{
		$data =  $this->input->get();
        $rec['data'] = $this->Laporan_model->getLapPenjualanGrid($data);
		$rec['cabang'] = (!empty($data['cabang'])&&!empty($rec['data'])) ? $rec['data'][0]['_nama_cab'] : "";

		$this->load->view('data/laporan/excelLapPenj',$rec);

	}
	
}