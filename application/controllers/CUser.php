<?php
header('Access-Control-Allow-Origin: *');
defined('BASEPATH') OR exit('No direct script access allowed');

class CUser extends CI_Controller {

	/**
	 * Index Page for this controller.
	 *
	 * Maps to the following URL
	 * 		http://example.com/index.php/welcome
	 *	- or -
	 * 		http://example.com/index.php/welcome/index
	 *	- or -
	 * Since this controller is set as the default controller in
	 * config/routes.php, it's displayed at http://example.com/
	 *
	 * So any other public methods not prefixed with an underscore will
	 * map to /index.php/welcome/<method_name>
	 * @see https://codeigniter.com/user_guide/general/urls.html
	 */
	function __construct()
	{
        parent::__construct();
        $this->load->library('form_validation');
        if (!$this->session->has_userdata('id')) {
			redirect(base_url()."login",'refresh');

		}

	}
	public function index()
	{
		$this->load->model('User_model');
		$this->load->model('Cabang_model');
        $data['data'] = $this->User_model->getUser();
        $data['cbCabang'] = $this->Cabang_model->cbCabang();
        $menu['menu'] = 'user';
		$this->load->view('template/top',$menu);
		$this->load->view('data/user',$data);
		$this->load->view('template/bot');
	}
	public function edit()
	{
		// return ajax for form data on edit button
		$id = $this->input->get('id');
		$this->load->model('User_model');
		$data = $this->User_model->edit($id);
		echo json_encode($data);
	}
	public function submit()
	{

		$this->load->model('User_model');
		$data = array(
	        '_alamat' => $this->input->post('alamat'),
	        '_group' => $this->input->post('group'),
	        '_id_cabang' => $this->input->post('id_cabang'),
	        '_nama' => $this->input->post('nama'),
	        '_pass' => $this->input->post('pass'),
	        '_telp' => $this->input->post('telp'),
	        '_username' => $this->input->post('username')
         );
		if (empty($this->input->post('id')) && !is_numeric($this->input->post('id'))) {
			$this->form_validation->set_rules('username', 'Username','is_unique[user_._username]',
	        									array('is_unique' => '%s. sudah tersedia'));
	        if ($this->form_validation->run() == FALSE)
	        {
	                $this->index();
	        }else{
				$data = $this->User_model->insert($data);
				if ($data) {
					redirect(base_url()."user/",'refresh');
				}
			}
		}else{
		    $data['_id'] = $this->input->post('id');
			$data = $this->User_model->update($data);
			if ($data) {
				redirect(base_url()."user/",'refresh');
			}
		}
	}
	public function delete()
	{
		$id = array('_id'=>$this->input->get('id'));
		$this->load->model('User_model');
		echo $this->User_model->delete($id);
		
	}
}
