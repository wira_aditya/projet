
$('.btn-edit').click(function() {
	$('#myModal').modal('show');
	var id = $(this).attr('data-id');
	console.log(id);
	$.get( "edit",{'id':id}, function( data ) {
	  data = jQuery.parseJSON(data);
	  for(var key in data){
	  	name = key.replace("_","");
	  	console.log($('[name="'+name+'"]').val());
	  	$('[name="'+name+'"]').val(data[key]);
	  }

	});	
});

$('#myModal').on('hidden.bs.modal', function () {
	$('form')[0].reset();
});

$('.btn-delete').click(function(){
	var id = $(this).attr('data-id');

	swal({
	  title: "",
	  text: "Apakah anda yakin menghapus data ini ? ",
	  type: "warning",
	  showCancelButton: true,
	  closeOnConfirm: false,
	  confirmButtonColor: "#DD6B55",
	  cancelButtonText: "Batal",
  	  confirmButtonText: "Ya",
	  showLoaderOnConfirm: true,
	},
	function(){
		console.log(id);
	 	$.get( "delete",{'id':id}, function( data ) {
		  console.log(data);
		  if(data){
		  	location.reload();	
		  }else{
		  	swal("Gagal","terjadi masalah saat menghapus data", "error")
		  }		  
		});
	});

});

$("#groupUser").change(function(argument) {
	console.log(this.value);
	if (this.value == 3) {
		$("#cbCabang").show();
	}else{
		$("#cbCabang").hide();	
	}
})


function loadBarangRow(data) {
	console.log(data);
	$.get( "transaksi/barangload",{'id':data.id ,'kode':data.kode}, function( data ) {
		console.log(data);
	  if (data !=0) {
		  data = jQuery.parseJSON(data);
		  console.log(data);
		  for(var key in data){
		  	name = key.replace("_","");
		  	console.log($('[name="'+name+'"]').val());
		  	$('[name="'+name+'"]').val(data[key]);
		  }
		  $("#modalBarang").modal("hide");
		$("#addTrBrg").removeAttr("disabled");
	  }else{
	  	$("#productTrans")[0].reset();
		$("#addTrBrg").attr("disabled","disabled");
	  }
	});
}

$("#BrgTransaksi tr").on("dblclick",function(e) {
	// alert("asds");
	id = $(this).attr("data-id");
	data = {kode:"",id:id};
	console.log("asd");
	loadBarangRow(data)
	// $("#modalBarang").modal("hide");
	// $("#addTrBrg").removeAttr("disabled");
});
$("#kode").on("blur",function(e) {
	kode = $(this).val();
	// console.log(id);
	data = {kode:kode,id:""};

	loadBarangRow(data)
	
});
$("#btnPilih").on("click",function(e){
	id = $("#BrgTransaksi .selected").attr("data-id");
	data = {kode:"",id:id};
	loadBarangRow(data)
	// $("#modalBarang").modal("hide");
	// $("#addTrBrg").removeAttr("disabled");	
});
function loadTableTR(data) {
	data = data.data;
	$('#tbdytrans').empty();
	total = 0;
	$.each(data,function(i,val){
		no = $('#tbdytrans>tr').length + 1;
		$('#tbdytrans').append("<tr><td class='textRight'>"+no+"</td><td>"+val._kode+"</td><td>"+val._nama+"</td><td class='textRight'>"+val._qty+"</td><td class='textRight'>"+val._harga+"</td><td class='textRight'>"+val._disk+"</td><td class='textRight'>"+val._total+"</td><td><button type='button' class='btn btn-danger btn-edit' onclick='deleteRow("+val._id_detail+")' data-id="+val._id_detail+"><i class='fa fa-times' aria-hidden='true'></i></button></td></tr>")
		total += parseInt(val._total);
	})
	$('#gtBayarTr').val(total)
	$('#gt').text(total)
	  				
}
$("#addTrBrg").on("click",function(e){
	// alert($("[name='qty']").val());
	if ($("[name='qty']").val() !== "" && $("[name='qty']").val() !== "0") {
		$("#error .error-notif").remove();
		console.log($('#productTrans').serialize());
		data = $('#productTrans').serialize();
		$.ajax({
			url:'CTrans/addTr',
			method:'post',
			data:data,
			success:function (res) {
	  			res = jQuery.parseJSON(res);
				console.log(res);
				console.log(res.success);
	  			if(res.success){
	  				loadTableTR(res);
	  			}
			}	
		});
		
	}else{
		if (!($("#error div").hasClass("error-notif"))) {
			$("#error").append("<div class='error-notif'><span>Qty tidak boleh 0 atau kosong</span></div>");
		}
	}

})

$("#bayarTr").keyup(function(){
	console.log($("#bayarTr").val());
	if(parseInt($('#gtBayarTr').val())<parseInt($("#bayarTr").val())){
		kembali = $("#bayarTr").val()-$('#gtBayarTr').val()
		$("#kembaliTr").val(kembali);
	}else{
		$("#kembaliTr").val(0);
	}
});

function deleteRow(rowId) {
	$.get( "transaksi/row-delete",{'id':rowId}, function( data ) {
	  data = jQuery.parseJSON(data);
	  if (data.success) {
	  	loadTableTR(data);
	  }
	 

	});	

}
$("#simpanBayar").on("click",function(){
	val = $("#fr-bayar").serialize();
	val += '&notr='+$('#no_tr').val();
	gt = parseInt($('#gtBayarTr').val());
	if (gt) {
		$.ajax({
			url:'transaksi/submit-trans',
			method:'post',
			data:val,
			success:function (res) {
	  			res = jQuery.parseJSON(res);
				console.log(res);
	  			if(res.success){
					window.open("transaksi/cetak?"+val);
	  				location.reload();
	  			}else{
	  				console.log("false");
	  			}
			}	
		});
	}
});

// cetak
$('#cetakLapStok').click(function(){
	data = $("#lapStok").serialize();
	window.open("cetak?"+data);
});
$('#cetakLapPenj').click(function(){
	data = $("#lapPenj").serialize();
	window.open("cetak?"+data);
});
$('#cetakLapRml').click(function(){
	data = $("#lapRml").serialize();
	window.open("cetak?"+data);
});



$('#excelLapStok').click(function(){
	data = $("#lapStok").serialize();
	window.open("excel?"+data);
});
$('#excelLapPenj').click(function(){
	data = $("#lapPenj").serialize();
	window.open("excel?"+data);
});
$('#excelLapRml').click(function(){
	data = $("#lapRml").serialize();
	window.open("excel?"+data);
});



