$(function () {
    $('#chart1').highcharts({
        chart: {
        plotBackgroundColor: null,
        plotBorderWidth: null,
        plotShadow: false,
        type: 'pie'
        },
        title: {
            text: 'Browser market shares January, 2015 to May, 2015'
        },
        tooltip: {
            pointFormat: '{series.name}: <b>{point.percentage:.1f}%</b>'
        },
        plotOptions: {
            pie: {
                allowPointSelect: true,
                cursor: 'pointer',
                dataLabels: {
                    enabled: true,
                    format: '<b>{point.name}</b>: {point.percentage:.1f} %',
                    style: {
                        color: (Highcharts.theme && Highcharts.theme.contrastTextColor) || 'black'
                    }
                }
            }
        },
        series: [{
            name: 'Potongan rambut Brands',
            colorByPoint: true,
            data: [{
                name: 'Layered Undercut',
                y: 20
            }, {
                name: 'Potongan rambut Top Knot',
                y: 25
            }, {
                name: 'Suavecito Premium Blends',
                y: 15
            }, {
                name: 'Chief Blue Waterbased Pomade',
                y:10
            }, {
                name: 'King Pomade',
                y:30,
                sliced: true,
                selected: true
            }]
        }]

    });
});