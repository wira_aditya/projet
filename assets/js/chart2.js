$(function () {
    $('#chart2').highcharts({
        chart: {
        type: 'column'
    },
    title: {
        text: 'Penjualan bulan MARET'
    },
    xAxis: {
        categories: [
            1,2,3,4,5,6,7,8,9,10,11,12,13,14,15,16,17,18,19,20,21,22,23,24,25,26,27,28,29,30
        ],
        crosshair: true
    },
    yAxis: {
        min: 0,
        title: {
            text: 'Rainfall (mm)'
        }
    },
    tooltip: {
        headerFormat: '<span style="font-size:10px">{point.key}</span><table>',
        pointFormat: '<tr><td style="color:{series.color};padding:0">{series.name}: </td>' +
            '<td style="padding:0"><b>{point.y:.1f} mm</b></td></tr>',
        footerFormat: '</table>',
        shared: true,
        useHTML: true
    },
    plotOptions: {
        column: {
            pointPadding: 0.2,
            borderWidth: 0
        }
    },
    series: [{
        name: 'Barang',
        data: [76,228,135,236,28,151,161,5,183,16,197,107,297,162,239,224,162,129,272,189,153,135,252,214,54,234,210,51,297,100]

        }, 
        {
        name: 'Jasa',
        data: [47,133,27,188,182,167,24,289,9,146,2,241,29,176,267,23,68,59,248,28,28,83,95,198,288,213,152,35,101,45]

    }]

    });
});