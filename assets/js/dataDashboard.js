function load_dash(data = "") {
	Highcharts.setOptions({
        global: {
            useUTC: false,
            
        },
        lang: {
          decimalPoint: ',',
          thousandsSep: '.'
        }
    });
	url = window.location.origin+'/'+window.location.pathname.split( '/' )[1];
	$.ajax({
			url:url+'/CChartApi/dashboard',
			method:'get',
			data:data,
			success:function (res) {
				console.log(accounting.formatMoney(100000000, "", 0, ".", ","));
	  			// res = jQuery.parseJSON(res);

// console.log(res.pie.data);
	  			// 
	  			// pie chart
	  			// 
	  			res.pieBarang.data = (res.pieBarang.data.length>0) ? res.pieBarang.data : [];
	  			res.pieJasa.data = (res.pieJasa.data.length>0) ? res.pieJasa.data : [];
	  			$('#chartPie1').highcharts({
		        chart: {
			        plotBackgroundColor: null,
			        plotBorderWidth: null,
			        plotShadow: false,
			        type: 'pie'
			        },
			        title: {
			            text: 'Data 5 Barang Terlaris Bulan '+res.bulan
			        },
			        tooltip: {
			            pointFormat: '{series.name}: <b>{point.y:1f}</b>'
			        },
			        plotOptions: {
			            pie: {
			                allowPointSelect: true,
			                cursor: 'pointer',
			                dataLabels: {
			                    enabled: true,
			                    format: '<b>{point.name}</b>: {point.y:1f} ',
			                    style: {
			                        color: (Highcharts.theme && Highcharts.theme.contrastTextColor) || 'black'
			                    }
			                }
			            }
			        },
			        series: [{
			            name: 'Jumlah',
			            colorByPoint: true,
			            data: res.pieBarang.data
			        }]

		    	});
		    	$('#chartPie2').highcharts({
		        chart: {
			        plotBackgroundColor: null,
			        plotBorderWidth: null,
			        plotShadow: false,
			        type: 'pie'
			        },
			        title: {
			            text: 'Data 5 Jasa Terlaris Bulan '+res.bulan
			        },
			        tooltip: {
			            pointFormat: '{series.name}: <b>{point.y:1f}</b>'
			        },
			        plotOptions: {
			            pie: {
			                allowPointSelect: true,
			                cursor: 'pointer',
			                dataLabels: {
			                    enabled: true,
			                    format: '<b>{point.name}</b>: {point.y:1f} ',
			                    style: {
			                        color: (Highcharts.theme && Highcharts.theme.contrastTextColor) || 'black'
			                    }
			                }
			            }
			        },
			        series: [{
			            name: 'Jumlah',
			            colorByPoint: true,
			            data: res.pieJasa.data
			        }]

		    	});
				$('#chart2').highcharts({
			        chart: {
			        	type: 'column'
				    },
				    title: {
				        text: ''
				    },
				    xAxis: {
				        categories: res.bar.bulan,
				        crosshair: true
				    },
				    yAxis: {
				        min: 0,
				        title: {
				            text: 'Jumlah'
				        }
				    },
				    tooltip: {
				        headerFormat: '<span style="font-size:10px">{point.key}</span><table>',
				        pointFormat: '<tr><td style="color:{series.color};padding:0">{series.name}: </td>' +
				            '<td style="padding:0"><b> Rp.{point.y:,.0f} </b></td></tr>',
				        footerFormat: '</table>',
				        shared: true,
				        useHTML: true
				    },
				    plotOptions: {
				        column: {
				            pointPadding: 0.2,
				            borderWidth: 0
				        }
				    },
				    series: [{
				        name: 'Barang',
				        data: res.bar.data.barang

				        }, 
				        {
				        name: 'Jasa',
				        data: res.bar.data.jasa

				    }]

			    });
			$("#produk-card").text(accounting.formatMoney(parseInt(res.card.produk), "", 0, ".", ","));
			$("#transaksi-card").text(accounting.formatMoney(parseInt(res.card.transaksi), "", 0, ".", ","));
			$("#pendapatan-card").text(accounting.formatMoney(parseInt(res.card.pendapatan), "", 0, ".", ","));

			}	

		});

}