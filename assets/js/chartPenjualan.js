function loadChart(argument = "") {
	// bar chart
	Highcharts.setOptions({
        global: {
            useUTC: false,
            
        },
        lang: {
          decimalPoint: ',',
          thousandsSep: '.'
        }
    });
    $.getJSON('http://localhost/project/CChartApi/penjualanLine', argument, function(chartData) {

	    $('#lineChartPenjualan').highcharts({
		    chart: {
		        type: 'line'
		    },
		    title: {
		        text: 'Penjualan Bulan '+chartData.bulan
		    },
		    xAxis: {
		        categories: chartData.tgl
		    },
		    yAxis: {
		        title: {
		            text: 'Total Transaksi'
		        }
		    },
		    tooltip: {
		        headerFormat: '<span style="font-size:10px">Tanggal:{point.key}</span><table>',
		        pointFormat: '<tr><td style="color:{series.color};padding:0">{series.name}: </td>' +
		            '<td style="padding:0"><b>Rp.{point.y:,.0f}</b></td></tr>',
		        footerFormat: '</table>',
		        shared: true,
		        useHTML: true
	    	},
		    plotOptions: {
		        line: {
		            dataLabels: {
		                enabled: true
		            }
		        }
		    },
		    series: [{
		        name: 'Total Penjualan',
		        data: chartData.data
		    }]
		});
	});

	$.getJSON('http://localhost/project/CChartApi/penjualanBar', argument, function(chartData) {
	    $('#barChartPenjualan').highcharts({
	        chart: {
	        type: 'column'
	    },
	    title: {
	        text: 'Data Penjualan Per Cabang Bulan'+chartData.bulan
	    },
	    xAxis: {
	        categories: chartData.cab,
	        crosshair: true
	    },
	    yAxis: {
	        min: 0,
	        title: {
	            text: 'Total Transaksi'
	        }
	    },
	    tooltip: {
	        headerFormat: '<span style="font-size:10px">{point.key}</span><table>',
	        pointFormat: '<tr><td style="color:{series.color};padding:0">{series.name}: </td>' +
	            '<td style="padding:0"><b>Rp.{point.y:,.0f}</b></td></tr>',
	        footerFormat: '</table>',
	        shared: true,
	        useHTML: true
	    },
	    plotOptions: {
	        column: {
	            pointPadding: 0.2,
	            borderWidth: 0
	        }
	    },
	    series: [{
	    	name:"Total Transaksi",
	        data: chartData.data
	    }]

	    });
	});

	$.getJSON('http://localhost/project/CChartApi/penjualanBar2', argument, function(chartData) {
	    $('#barChartPenjualan2').highcharts({
	        chart: {
	        type: 'column'
	    },
	    title: {
	        text: 'Data Penjualan Per Cabang Bulan '+chartData.bulan+' Berdasarkan Jenis Produk'
	    },
	    xAxis: {
	        categories: chartData.cab,
	        crosshair: true
	    },
	    yAxis: {
	        min: 0,
	        title: {
	            text: 'Total Transaksi'
	        }
	    },
	    tooltip: {
	        headerFormat: '<span style="font-size:10px">{point.key}</span><table>',
	        pointFormat: '<tr><td style="color:{series.color};padding:0">{series.name}: </td>' +
	            '<td style="padding:0"><b>Rp.{point.y:,.0f}</b></td></tr>',
	        footerFormat: '</table>',
	        shared: true,
	        useHTML: true
	    },
	    plotOptions: {
	        column: {
	            pointPadding: 0.2,
	            borderWidth: 0
	        }
	    },
	    series: [{
				        name: 'Barang',
				        data: chartData.data.barang

				        }, 
				        {
				        name: 'Jasa',
				        data: chartData.data.jasa

				    }]

	    });
	});
	// line chart


}